<?php

namespace Drupal\opin_performance;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Drupal\Core\DependencyInjection\ServiceProviderInterface;
use Drupal\opin_performance\Asset\OpinCssOptimizer;

class OpinPerformanceServiceProvider extends ServiceProviderBase implements ServiceProviderInterface {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    $container->getDefinition('advagg.optimizer.css')
      ->setClass(OpinCssOptimizer::class);
  }

}
