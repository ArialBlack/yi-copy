<?php

namespace Drupal\opin_performance\Asset;

use Drupal\advagg\Asset\AssetOptimizationEvent;
use Drupal\advagg\Asset\CssOptimizer;

class OpinCssOptimizer extends CssOptimizer {
  /**
   * {@inheritdoc}
   */
  protected function optimizeFile(array &$asset, array $data) {
    $contents = $this->updateUrls($data['contents'], $asset['data']);
    if ($this->config->get('css.combine_media') && $asset['media'] !== 'all') {
      $contents = "@media {$asset['media']}{{$contents}}";
      // Original service resets the assed media attribute, skip that for test.
    }
    $asset_event = new AssetOptimizationEvent($contents, $asset, $data);
    $this->eventDispatcher->dispatch(AssetOptimizationEvent::CSS, $asset_event);
    $contents = $asset_event->getContent();
    $asset = $asset_event->getAsset();

    // If file contents are unaltered return FALSE.
    if ($contents === $data['contents'] && !$this->gZip) {
      return FALSE;
    }
    return $this->writeFile($contents, $data['cid']);
  }

}
