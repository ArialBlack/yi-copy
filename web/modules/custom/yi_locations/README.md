# YI Locations

Allows to build custom maps with image file, leaflet.js and custom markers.

This module attaches required leaflet.js libs and custom js and css files to custom block 'location_map'.

User can add block at Layout builder, upload image and add several location items (paragraphs type 'location_map_item').

Each location have next fields: Title (Text,plain), Description(Text, formatted, long) and Longitude with latitude. To get coordinates, there is help text under field: "Click on the map. Coordinates will be copied into the clipboard; Then paste into this field."
