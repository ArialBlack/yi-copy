(function ($) {
  Drupal.behaviors.yiLocationsMap = {
    attach: function (context, settings) {
      $('.block--location-map img', context).once('yiLocationsMap').each(function (e) {
        var map;
        var mapInitilized = false;

        var rebuildMap = function() {
          if (mapInitilized) {
            map.invalidateSize();
            window.dispatchEvent(new Event('resize'));
          }
        };

        var resizeObserver = new ResizeObserver(entries => rebuildMap());
        resizeObserver.observe(document.body);

        if (drupalSettings.yiLocationsType === 'contact-us') {
          $(this).closest('.block--location-map').addClass('block--location-map--contact-us');
        }

        $.widget("wgm.imgNotes2", $.wgm.imgViewer2, {
          options: {
            /*
             *  Default action for addNote callback
            */
            addNote: function (data) {
              map = this.map;
              var loc = this.relposToLatLng(data.x, data.y);
              var popup = L.responsivePopup({ hasTip: true, autoPan: false}).setContent(data.note);
              var icon = L.BeautifyIcon.icon({
                className: 'custom-div-icon',
                html: "<div class='yi-map-pin'><i></i></div>",
                iconSize: [40, 40],
              });

              var marker = L.marker(loc, {icon: icon}).addTo(map).bindPopup(popup);

              map.touchZoom.disable();
              map.doubleClickZoom.disable();
              map.scrollWheelZoom.disable();
              map.boxZoom.disable();
              map.keyboard.disable();
              map.dragging.disable();
              $(".leaflet-control-zoom").css("visibility", "hidden");
              mapInitilized = true;

              marker.on("popupopen", function (e) {
                $('.leaflet-popup-pane').addClass('opened');
                var $container = $('.viewport.leaflet-container');

                if ($(window).width() >= 768) {
                  if ($(window).width() >= 768 && $(window).width() < 1024) {
                    $('.leaflet-popup-pane.opened').css({
                      'width': '430px',
                      'height': $container.height(),
                      left: $container.width() - 430 + 'px'
                    });
                  } else {
                    $('.leaflet-popup-pane.opened').css({
                      'width': '330px',
                      'height': $container.height(),
                      left: $container.width() - 330 + 'px'
                    });
                  }

                }
                else {
                  var _h = 0;
                  setTimeout(function () {
                    _h = $('.leaflet-popup-pane.opened .leaflet-popup').height() + 20 + 'px';
                    $('.leaflet-popup-pane.opened').css({
                      'width': $container.width() + 'px',
                      'height': _h,
                      'background-color': '#53D3D1'
                    });
                  }, 250);
                }

                setTimeout(function () {
                  rebuildMap();
                }, 100);
              });

              marker.on("popupclose", function (e) {
                $('.leaflet-popup-pane.opened').css({
                  'width': '',
                  'height': '',
                  'background-color': ''
                });

                $('.leaflet-popup-pane').removeClass('opened');

                $('.block--location-map .field-content').css({
                  'border-top': ''
                });

                setTimeout(function () {
                  rebuildMap();
                }, 100);
              });
            }

          },

          /*
           *  Add notes from a javascript array
           */
          import: function (notes) {
            if (this.ready) {
              var self = this;
              $.each(notes, function () {
                self.options.addNote.call(self, this);
              });
            }
          }
        });

        var notes = drupalSettings.yiLocations;

        var $img = $(this).imgNotes2( {
          onReady: function () {
            this.import(notes);

            // Auto-open first location at the 'Contact us' map
            $('.leaflet-marker-pane .beautify-marker:first-child').click();
          }
        });

      });
    }
  };
}(jQuery, drupalSettings));
