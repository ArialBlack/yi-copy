(function ($) {
  Drupal.behaviors.yiLocationsBuild = {
    attach: function (context, settings) {
      $('.field--name-field-map img', context).once('yiLocationsBuild').click(function (e) {
        let copyUrl2Clipboard = function (copyText) {
          document.addEventListener('copy', function (e) {
            e.clipboardData.setData('text/plain', copyText);
            e.preventDefault();
          }, true);

          document.execCommand('copy');
          alert('Coordinates were copied to the clipboard: ' + copyText);
        };

        let parentOffset = $(this).offset();
        let relX = e.pageX - parentOffset.left;
        let relY = e.pageY - parentOffset.top;
        let width = $(this).width();
        let height = $(this).height();
        let xy = relX / width + ';' + relY / height;
        copyUrl2Clipboard(xy);
      });
    }
  };
}(jQuery));
