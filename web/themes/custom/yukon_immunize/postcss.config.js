module.exports = {
  plugins: {
    'postcss-easy-import': {},
    'postcss-mixins': {},
    'postcss-nested': {},
    'tailwindcss': {},
    'autoprefixer': {},
    'postcss-combine-media-query': {},
    'postcss-extract-media-query': {
      queries: {
        '(min-width: 375px)': 'min-width-375',
        '(min-width: 480px)': 'min-width-480',
        '(min-width: 768px)': 'min-width-768',
        '(min-width: 1024px)': 'min-width-1024',
        '(min-width: 1280px)': 'min-width-1280',
      },
      extractAll: false,
      output: {
        path: 'dist/split',
        name: '[name]-[query].[ext]',
      }
    },
  },
}
