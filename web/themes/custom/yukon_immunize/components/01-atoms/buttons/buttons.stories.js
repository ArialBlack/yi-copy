import Button from './button.twig';
import buttonData from './button.yml';

export default { title: 'Atoms/Button' };

export const Default = () => (
  Button(buttonData)
);