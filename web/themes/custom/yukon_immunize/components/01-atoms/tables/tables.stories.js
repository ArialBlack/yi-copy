import tables from './tables.twig';
import tablesData from './tables.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Atoms/Tables' };

export const table = () => (
  tables(tablesData)
);
