import Link from './link.twig';
import LinkButton from './link-button.twig';
import linkData from './link.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Atoms/Links' };

export const Default = () => (
  Link(linkData)
);

export const Button = () => (
  LinkButton(linkData)
);
