import checkbox from './checkbox/checkbox.twig';
import radio from './radio/radio.twig';
import select from './select/select.twig';
import textfield from './textfield/textfield.twig';
import multiSelect from './multiselect/multiselect.twig';
import textArea from './textarea/textarea.twig';

import checkboxData from './checkbox/checkbox.yml';
import radioData from './radio/radio.yml';
import selectOptionsData from './select/select.yml';
import textfieldData from './textfield/textfield.yml';
import multiSelectOptionsData from './multiselect/multiselect.yml';
import textAreaData from './textarea/textarea.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Atoms/Form Elements' };

export const checkboxes = () => (
  checkbox(checkboxData)
);

export const radioButtons = () => (
  radio(radioData)
);

export const selectList = () => (
  select(selectOptionsData)
);

export const singleTextfield = () => (
  textfield(textfieldData)
);

export const multiSelectList = () => (
  multiSelect(multiSelectOptionsData)
);

export const textarea = () => (
  textArea(textAreaData)
);
