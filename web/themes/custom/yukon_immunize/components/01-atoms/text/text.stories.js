import heading from './headings/headings.twig';
import blockquoteTwig from './blockquote/blockquote.twig';
import pre from './pre/pre.twig';
import wysiwyg from './wysiwyg/wysiwyg.twig';
import plainText from './plain-text/plain-text.twig';
import introText from './intro-text/intro-text.twig';

import blockquoteData from './blockquote/blockquote.yml';
import headingsData from './headings/headings.yml';
import plainTextData from './plain-text/plain-text.yml';
import introTextData from './intro-text/intro-text.yml';
import preData from './pre/pre.yml';
import wysiwygData from './wysiwyg/wysiwyg.yml'

/**
 * Storybook Definition.
 */
export default { title: 'Atoms/Text' };

// Loop over items in headingData to show each one in the example below.
const headingData = headingsData.map((d) => heading(d)).join('');

export const headings = () => (
  headingData
);

export const blockquote = () => (
  blockquoteTwig(blockquoteData)
);

export const preformatted = () => (
  pre(preData)
);

export const WYSIWYG = () => (
  wysiwyg(wysiwygData)
);

export const PlainText = () => (
  plainText(plainTextData)
);

export const IntroText = () => (
  introText(introTextData)
);
