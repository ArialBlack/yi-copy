// Component attachment to Storybook

/* If your component needs AlpineJS. */
// import Alpine from 'alpinejs';

/* If your component needs Drupal attributes. */
// import drupalAttribute from 'drupal-attribute'

import BasicPageSimpleTwig from './basic-page-simple.twig';
import BasicPageSimpleCode from '!raw-loader!./basic-page-simple.twig';
import BasicPageSimpleData from './basic-page-simple.yml';
import BasicPageSimpleDocs from './basic-page-simple.mdx';

export default {
  title: 'Templates/Content/Basic Page Simple',
  parameters: { 
    docs: { 
      source: {
        code: BasicPageSimpleCode
      },
      page: BasicPageSimpleDocs
    } 
  },
};

export const BasicPageSimple = () => (
  BasicPageSimpleTwig(BasicPageSimpleData)
);
