import screens from './screens.twig';
import screensData from './screens.yml';

export default { title: 'Base/Breakpoints & Container Sizes' };

export const Screens = () => (
  screens(screensData)
);
