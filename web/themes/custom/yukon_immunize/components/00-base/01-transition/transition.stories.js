import transition from './transition.twig';
import transitionData from './transition.yml';

/**
 * Add storybook definition for Animations.
 */
export default { title: 'Base/Transition' };

export const Usage = () => (
  transition(transitionData)
);
