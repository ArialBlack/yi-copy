// Component attachment to Storybook

/* If your component needs AlpineJS. */
// import Alpine from 'alpinejs';

/* If your component needs Drupal attributes. */
// import drupalAttribute from 'drupal-attribute'

import SearchResultsSimpleTwig from './search-results-simple.twig';
import SearchResultsSimpleData from './search-results-simple.yml';
import SearchResultsSimpleDocs from './search-results-simple.mdx';

export default {
  title: 'Drupal/Search Results/Search Results Simple',
  parameters: { 
    docs: { 
      page: SearchResultsSimpleDocs
    } 
  },
};

export const SearchResultsSimple = () => (
  SearchResultsSimpleTwig(SearchResultsSimpleData)
);
