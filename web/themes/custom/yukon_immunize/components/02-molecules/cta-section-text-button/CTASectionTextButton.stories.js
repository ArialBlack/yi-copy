import CTASectionTextButtonTwig from './cta-section-text-button.twig';
import CTASectionTextButtonData from './cta-section-text-button.yml';

export default { title: 'Molecules/CTA Section/CTA Section Text Button' };

export const Primary = () => (
  CTASectionTextButtonTwig(CTASectionTextButtonData)
);
