// Component attachment to Storybook
import Alpine from 'alpinejs'; // If your component needs AlpineJS.
// import drupalAttribute from 'drupal-attribute' // If your component needs Drupal attributes.
import navigationSimpleLinksTwig from './navigation-simple-links.twig';
import navigationSimpleLinksData from './navigation-simple-links.yml';

export default { title: 'Molecules/Navigation/Navigation Simple Links' };

export const Primary = () => (
  navigationSimpleLinksTwig(navigationSimpleLinksData)
);
