// Component attachment to Storybook

/* If your component needs AlpineJS. */
// import Alpine from 'alpinejs';

/* If your component needs Drupal attributes. */
// import drupalAttribute from 'drupal-attribute'

import UtilitySimpleLinksTwig from './utility-simple-links.twig';
import UtilitySimpleLinksData from './utility-simple-links.yml';

export default {
  title: 'Molecules/Navigation/Utility Simple Links',
};

export const UtilitySimpleLinks = () => (
  UtilitySimpleLinksTwig(UtilitySimpleLinksData)
);
