// Component attachment to Storybook
import Alpine from 'alpinejs'; // If your component needs AlpineJS.
// import drupalAttribute from 'drupal-attribute' // If your component needs Drupal attributes.
import NavigationMegamenuLinksTwig from './navigation-megamenu-links.twig';
import NavigationMegamenuLinksData from './navigation-megamenu-links.yml';

export default { title: 'Molecules/Navigation/Navigation Megamenu Links' };

export const Primary = () => (
  NavigationMegamenuLinksTwig(NavigationMegamenuLinksData)
);
