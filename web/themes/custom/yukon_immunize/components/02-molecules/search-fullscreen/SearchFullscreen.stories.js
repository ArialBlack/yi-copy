// Component attachment to Storybook
// import Alpine from 'alpinejs'; // If your component needs AlpineJS.
// import drupalAttribute from 'drupal-attribute' // If your component needs Drupal attributes.
import searchFullscreenTwig from './search-fullscreen.twig';
import searchFullscreenData from './search-fullscreen.yml';

export default { title: 'Molecules/Search/Search Fullscreen' };

export const Primary = () => (
  searchFullscreenTwig(searchFullscreenData)
);
