// Component attachment to Storybook

/* If your component needs AlpineJS. */
// import Alpine from 'alpinejs';

/* If your component needs Drupal attributes. */
// import drupalAttribute from 'drupal-attribute'

import HeaderMenusSimpleTwig from './header-menus-simple.twig';
import HeaderMenusSimpleData from './header-menus-simple.yml';
import HeaderMenusSimpleDocs from './header-menus-simple.mdx';

let args = {
  title: 'Organisms/Header/Header Menus Simple',
  parameters: { 
    docs: { 
      page: HeaderMenusSimpleDocs
    } 
  },
  argTypes: {
    config_term: {
      table: {
        disable: true
      },
    },
    site_logo: {
      table: {
        disable: true
      },
    },
    site_logo_mobile: {
      table: {
        disable: true
      },
    },
    form: {
      table: {
        disable: true
      },
    },
    form_mobile: {
      table: {
        disable: true
      },
    },
    menu: {
      table: {
        disable: true
      },
    },
    mobile_button_svg: {
      table: {
        disable: true
      },
    },
    mobile_close_svg: {
      table: {
        disable: true
      },
    },
  }
}

export default args;

export const Primary = (args) => (
  HeaderMenusSimpleTwig(args)
);

Primary.args = HeaderMenusSimpleData;
