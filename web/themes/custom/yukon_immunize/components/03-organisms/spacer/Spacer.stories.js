// Component attachment to Storybook

/* If your component needs AlpineJS. */
import Alpine from 'alpinejs';

/* If your component needs Drupal attributes. */
// import drupalAttribute from 'drupal-attribute'

import SpacerTwig from './spacer.twig';
import SpacerData from './spacer.yml';
import SpacerDocs from './spacer.mdx';

let args = {
  title: 'Organisms/Spacer/Spacer',
  parameters: { 
    docs: { 
      page: SpacerDocs
    } 
  },
  argTypes: {
    config_term: {
      table: {
        disable: true
      },
    },
  }
}

export default args;

export const Primary = (args) => (
  SpacerTwig(args)
);

Primary.args = SpacerData;
