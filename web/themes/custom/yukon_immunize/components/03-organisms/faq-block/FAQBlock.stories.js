import FAQBlockTwig from './faq-block.twig';
import FAQBlockCode from '!raw-loader!./faq-block.twig';
import FAQBlockData from './faq-block.yml';
import FAQBlockDocs from './faq-block.mdx';

const args = {
  title: "Organisms/FAQ/FAQ Blocks",
  parameters: {
    docs: {
      source: {
        code: FAQBlockCode
      },
      page: FAQBlockDocs
    }
  },
  argTypes: {
    checkbox: {
      name: "Two columns",
      description: 'Enable two columns',
      table: {
        type: {
          summary: 'bool',
          detail: 'whether the one column should be disabled'
        },
        defaultValue: {
          summary: false
        },
      },
    },
    title_color: {
      name: 'Title Color',
      description: '*parameter*: `FAQ Title Color`',
      defaultValue: "text",
      control: {
        type: 'select',
        options: ["text", "background", "primary", "secondary", "accent", "highlight", "warning", "error", "message"],
      },
      table: {
        type: {
          summary: 'enum',
          detail: 'text color of FAQ section title'
        },
        defaultValue: {
          summary: 'text'
        },
      },
    },
    q_text_color: {
      name: 'Questions Text Color',
      description: '*parameter*: `FAQ Questions Text Color`',
      defaultValue: "text",
      control: {
        type: 'select',
        options: ["text", "background", "primary", "secondary", "accent", "highlight", "warning", "error", "message"],
      },
      table: {
        type: {
          summary: 'enum',
          detail: 'text color of FAQ section questions'
        },
        defaultValue: {
          summary: 'text'
        },
      },
    },
    a_text_color: {
      name: 'Answers Text Color',
      description: '*parameter*: `FAQ Answers Text Color`',
      defaultValue: "accent",
      control: {
        type: 'select',
        options: ["text", "background", "primary", "secondary", "accent", "highlight", "warning", "error", "message"],
      },
      table: {
        type: {
          summary: 'enum',
          detail: 'text color of FAQ section answers'
        },
        defaultValue: {
          summary: 'accent'
        },
      },
    },
    arrow_color: {
      name: 'Arrow Color',
      description: '*parameter*: `FAQ Arrow Color`',
      defaultValue: "default",
      control: {
        type: 'select',
        options: ["default", "text", "background", "primary", "secondary", "accent", "highlight", "warning", "error", "message"],
      },
      table: {
        type: {
          summary: 'enum',
          detail: 'color of accordion arrows'
        },
        defaultValue: {
          summary: 'default'
        },
      },
    },
    config_term: {
      table: {
        disable: true
      },
    },
    title: {
      table: {
        disable: true
      },
    },
    faqs: {
      table: {
        disable: true
      },
    },
  }
}

export default args;

export const Primary = (args) => (
  FAQBlockTwig(args)
);

Primary.args = FAQBlockData;