// Component attachment to Storybook

/* If your component needs AlpineJS. */
// import Alpine from 'alpinejs';

/* If your component needs Drupal attributes. */
// import drupalAttribute from 'drupal-attribute'

import CTAHeadingWithButtonTwig from './cta-heading-with-button.twig';
import CTAHeadingWithButtonData from './cta-heading-with-button.yml';
import CTAHeadingWithButtonDocs from './cta-heading-with-button.mdx';

let args = {
  title: 'Organisms/Cta Block/CTA Heading with Button',
  parameters: { 
    docs: { 
      page: CTAHeadingWithButtonDocs
    } 
  },
  argTypes: {
    bg_color: {
      name: 'Background Color',
      defaultValue: 'primary',
      control: {
        type: 'select',
        options: {
          White: 'white',
          Black: 'black',
          Translucent: 'translucent',
          Primary: 'primary',
          PrimaryLight: 'primaryLight',
          Secondary: 'secondary',
          Accent: 'accent',
          Text: 'text',
          Background: 'background',
          Highlight: 'highlight',
          Warning: 'warning',
          Error: 'error',
          Message: 'message'
        }
      },
      table: {
        category: 'Settings'
      },
    },
    heading_color: {
      name: 'Heading Color',
      defaultValue: 'black',
      control: {
        type: 'select',
        options: {
          White: 'white',
          Black: 'black',
          Translucent: 'translucent',
          Primary: 'primary',
          PrimaryLight: 'primaryLight',
          Secondary: 'secondary',
          Accent: 'accent',
          Text: 'text',
          Background: 'background',
          Highlight: 'highlight',
          Warning: 'warning',
          Error: 'error',
          Message: 'message'
        }
      },
      table: {
        category: 'Settings'
      },
    },
    link_color: {
      name: 'Button Text Color',
      defaultValue: '#fff',
      control: {
        type: 'select',
        options: {
          'White': '#fff',
          'Black': '#000',
          'Primary': '#38b2ac',
          'Secondary': '#a0aec0',
          'Accent': '#4a5568',
          'Highlight': '#90cdf4',
          'Warning': '#faf089',
          'Error': '#feb2b2',
          'Message': '#9ae6b4'
        },
      },
      table: {
        category: 'Settings'
      },
    },
    link_color_hover: {
      name: 'Button Text Color (Hover/Focus)',
      defaultValue: '#000',
      control: {
        type: 'select',
        options: {
          'White': '#fff',
          'Black': '#000',
          'Primary': '#38b2ac',
          'Secondary': '#a0aec0',
          'Accent': '#4a5568',
          'Highlight': '#90cdf4',
          'Warning': '#faf089',
          'Error': '#feb2b2',
          'Message': '#9ae6b4'
        },
      },
      table: {
        category: 'Settings'
      },
    },
    link_bg_color: {
      name: 'Button Background Color',
      defaultValue: '#000',
      control: {
        type: 'select',
        options: {
          'White': '#fff',
          'Black': '#000',
          'Primary': '#38b2ac',
          'Secondary': '#a0aec0',
          'Accent': '#4a5568',
          'Highlight': '#90cdf4',
          'Warning': '#faf089',
          'Error': '#feb2b2',
          'Message': '#9ae6b4'
        },
      },
      table: {
        category: 'Settings'
      },
    },
    link_bg_color_hover: {
      name: 'Button Background Color (Hover/Focus)',
      defaultValue: '#fff',
      control: {
        type: 'select',
        options: {
          'White': '#fff',
          'Black': '#000',
          'Primary': '#38b2ac',
          'Secondary': '#a0aec0',
          'Accent': '#4a5568',
          'Highlight': '#90cdf4',
          'Warning': '#faf089',
          'Error': '#feb2b2',
          'Message': '#9ae6b4'
        },
      },
      table: {
        category: 'Settings'
      },
    },
    config_term: {
      table: {
        disable: true
      },
    },
    heading: {
      table: {
        disable: true
      },
    },
    link_url: {
      table: {
        disable: true
      },
    },
    link_text: {
      table: {
        disable: true
      },
    },
    heading_classes: {
      table: {
        disable: true
      },
    },
    button_hover_classes: {
      table: {
        disable: true
      },
    },
    button_classes: {
      table: {
        disable: true
      },
    }
  }
};

export default args;

export const Primary = (args) => (
  CTAHeadingWithButtonTwig(args)
);

Primary.args = CTAHeadingWithButtonData;
