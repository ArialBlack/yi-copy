// Component attachment to Storybook

/* If your component needs AlpineJS. */
// import Alpine from 'alpinejs';

/* If your component needs Drupal attributes. */
// import drupalAttribute from 'drupal-attribute'

import HeroBackgroundImageWithTextButtonTwig from './hero-background-image-with-text-button.twig';
import HeroBackgroundImageWithTextButtonData from './hero-background-image-with-text-button.yml';
import HeroBackgroundImageWithTextButtonDocs from './hero-background-image-with-text-button.mdx';

let args = {
  title: 'Organisms/Hero/Hero Background Image With Text Button',
  parameters: { 
    docs: { 
      page: HeroBackgroundImageWithTextButtonDocs
    } 
  },
  argTypes: {
    text_color: {
      name: 'Text Color',
      defaultValue: 'white',
      control: {
        type: 'select',
        options: {
          White: 'white',
          Black: 'black',
          Translucent: 'translucent',
          Primary: 'primary',
          PrimaryLight: 'primaryLight',
          Secondary: 'secondary',
          Accent: 'accent',
          Text: 'text',
          Background: 'background',
          Highlight: 'highlight',
          Warning: 'warning',
          Error: 'error',
          Message: 'message'
        }
      },
      table: {
        category: 'Settings',
        subcategory: 'Text'
      },
    },
    image_opacity: {
      name: 'Image Opacity',
      defaultValue: 25,
      control: {
        type: 'range',
        min: 0,
        max: 100,
        step: 25
      },
      table: {
        category: 'Settings',
        subcategory: 'Image'
      },
    },
    button_bg_color: {
      name: 'Button Background Color',
      defaultValue: '#38b2ac',
      control: {
        type: 'select',
        options: {
          'White': '#fff',
          'Black': '#000',
          'Primary': '#38b2ac',
          'Secondary': '#a0aec0',
          'Accent': '#4a5568',
          'Highlight': '#90cdf4',
          'Warning': '#faf089',
          'Error': '#feb2b2',
          'Message': '#9ae6b4'
        },
      },
      table: {
        category: 'Settings',
        subcategory: 'Buttons'
      },
    },
    button_text_color: {
      name: 'Button Text Color',
      defaultValue: '#fff',
      control: {
        type: 'select',
        options: {
          'White': '#fff',
          'Black': '#000',
          'Primary': '#38b2ac',
          'Secondary': '#a0aec0',
          'Accent': '#4a5568',
          'Highlight': '#90cdf4',
          'Warning': '#faf089',
          'Error': '#feb2b2',
          'Message': '#9ae6b4'
        },
      },
      table: {
        category: 'Settings',
        subcategory: 'Buttons'
      },
    },
    button_bg_color_hover: {
      name: 'Button Background Color',
      defaultValue: '#38b2ac',
      control: {
        type: 'select',
        options: {
          'White': '#fff',
          'Black': '#000',
          'Primary': '#38b2ac',
          'Secondary': '#a0aec0',
          'Accent': '#4a5568',
          'Highlight': '#90cdf4',
          'Warning': '#faf089',
          'Error': '#feb2b2',
          'Message': '#9ae6b4'
        },
      },
      table: {
        category: 'Settings',
        subcategory: 'Buttons'
      },
    },
    button_text_color_hover: {
      name: 'Button Text Color',
      defaultValue: '#fff',
      control: {
        type: 'select',
        options: {
          'White': '#fff',
          'Black': '#000',
          'Primary': '#38b2ac',
          'Secondary': '#a0aec0',
          'Accent': '#4a5568',
          'Highlight': '#90cdf4',
          'Warning': '#faf089',
          'Error': '#feb2b2',
          'Message': '#9ae6b4'
        },
      },
      table: {
        category: 'Settings',
        subcategory: 'Buttons'
      },
    },
    config_term: {
      table: {
        disable: true
      },
    }
  }
}

export default args;

export const HeroBackgroundImageWithTextButton = (args) => (
  HeroBackgroundImageWithTextButtonTwig(args)
);

HeroBackgroundImageWithTextButton.args = HeroBackgroundImageWithTextButtonData;
