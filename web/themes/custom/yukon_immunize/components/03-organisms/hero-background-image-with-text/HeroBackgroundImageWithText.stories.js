// Component attachment to Storybook

/* If your component needs AlpineJS. */
// import Alpine from 'alpinejs';

/* If your component needs Drupal attributes. */
// import drupalAttribute from 'drupal-attribute'

import HeroBackgroundImageWithTextTwig from './hero-background-image-with-text.twig';
import HeroBackgroundImageWithTextData from './hero-background-image-with-text.yml';
import HeroBackgroundImageWithTextDocs from './hero-background-image-with-text.mdx';

let args = {
  title: 'Organisms/Hero/Hero Background Image With Text',
  parameters: { 
    docs: { 
      page: HeroBackgroundImageWithTextDocs
    } 
  },
  argTypes: {
    text_color: {
      name: 'Text Color',
      defaultValue: 'white',
      control: {
        type: 'select',
        options: {
          White: 'white',
          Black: 'black',
          Translucent: 'translucent',
          Primary: 'primary',
          PrimaryLight: 'primaryLight',
          Secondary: 'secondary',
          Accent: 'accent',
          Text: 'text',
          Background: 'background',
          Highlight: 'highlight',
          Warning: 'warning',
          Error: 'error',
          Message: 'message'
        }
      },
      table: {
        category: 'Settings',
        subcategory: 'Text'
      },
    },
    image_opacity: {
      name: 'Image Opacity',
      defaultValue: 25,
      control: {
        type: 'range',
        min: 0,
        max: 100,
        step: 25
      },
      table: {
        category: 'Settings',
        subcategory: 'Image'
      },
    },
    config_term: {
      table: {
        disable: true
      },
    }
  }
}

export default args;

export const HeroBackgroundImageWithText = (args) => (
  HeroBackgroundImageWithTextTwig(args)
);

HeroBackgroundImageWithText.args = HeroBackgroundImageWithTextData;
