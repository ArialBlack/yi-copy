// Component attachment to Storybook

/* If your component needs AlpineJS. */
// import Alpine from 'alpinejs';

/* If your component needs Drupal attributes. */
// import drupalAttribute from 'drupal-attribute'

import CalloutHeadingWithTextTwig from './callout-heading-with-text.twig';
import CalloutHeadingWithTextData from './callout-heading-with-text.yml';
import CalloutHeadingWithTextDocs from './callout-heading-with-text.mdx';

let args = {
  title: 'Organisms/Callout/Callout Heading With Text',
  parameters: { 
    docs: { 
      page: CalloutHeadingWithTextDocs
    } 
  },
  argTypes: {
    background_color: {
      name: 'Background Color',
      defaultValue: 'accent',
      control: {
        type: 'select',
        options: {
          White: 'white',
          Black: 'black',
          Translucent: 'translucent',
          Primary: 'primary',
          PrimaryLight: 'primaryLight',
          Secondary: 'secondary',
          Accent: 'accent',
          Text: 'text',
          Background: 'background',
          Highlight: 'highlight',
          Warning: 'warning',
          Error: 'error',
          Message: 'message'
        }
      },
      table: {
        category: 'Settings'
      },
    },
    heading_color: {
      name: 'Heading Color',
      defaultValue: 'white',
      control: {
        type: 'select',
        options: {
          White: 'white',
          Black: 'black',
          Translucent: 'translucent',
          Primary: 'primary',
          PrimaryLight: 'primaryLight',
          Secondary: 'secondary',
          Accent: 'accent',
          Text: 'text',
          Background: 'background',
          Highlight: 'highlight',
          Warning: 'warning',
          Error: 'error',
          Message: 'message'
        }
      },
      table: {
        category: 'Settings',
        subcategory: 'Text'
      },
    },
    subtext_color: {
      name: 'Subtext Color',
      defaultValue: 'white',
      control: {
        type: 'select',
        options: {
          White: 'white',
          Black: 'black',
          Translucent: 'translucent',
          Primary: 'primary',
          PrimaryLight: 'primaryLight',
          Secondary: 'secondary',
          Accent: 'accent',
          Text: 'text',
          Background: 'background',
          Highlight: 'highlight',
          Warning: 'warning',
          Error: 'error',
          Message: 'message'
        }
      },
      table: {
        category: 'Settings',
        subcategory: 'Text'
      },
    },
    config_term: {
      table: {
        disable: true
      },
    },
    story: {
      table: {
        disable: true
      },
    }
  }
};

export default args;

export const CalloutHeadingWithText = (args) => (
  CalloutHeadingWithTextTwig(args)
);

CalloutHeadingWithText.args = CalloutHeadingWithTextData;
