import RelatedArticlesSimpleTwig from './related-articles-simple.twig';
import RelatedArticlesSimpleData from './related-articles-simple.yml';
import RelatedArticlesSimpleDocs from './related-articles-simple.mdx';

export default {
  title: 'Organisms/Articles/Related Articles Simple',
  parameters: { 
    docs: { 
      page: RelatedArticlesSimpleDocs
    } 
  },
  argTypes: {
    config_term: {
      table: {
        disable: true
      },
    },
  }
};

export const RelatedArticlesSimple = () => (
  RelatedArticlesSimpleTwig(RelatedArticlesSimpleData)
);
