// Component attachment to Storybook

/* If your component needs AlpineJS. */
// import Alpine from 'alpinejs';

/* If your component needs Drupal attributes. */
// import drupalAttribute from 'drupal-attribute'

import BreadcrumbsTwig from './breadcrumbs.twig';
import BreadcrumbsData from './breadcrumbs.yml';
import BreadcrumbsDocs from './breadcrumbs.mdx';

const args = {
  title: 'Organisms/Breadcrumbs/Breadcrumbs',
  parameters: { 
    docs: { 
      page: BreadcrumbsDocs
    } 
  },
  argTypes: {
    'fullWidth': { control: 'boolean' },
    'showHome': { control: 'boolean' },
    'dividerType': {
      control: {
        type: 'select',
        options: [
          'chevrons', 
          'slashes', 
          'none'
        ],
      },
    },
    config_term: {
      table: {
        disable: true
      },
    },
  },
};

export default args;

export const Primary = (args) => (
  BreadcrumbsTwig(args)
);

Primary.args = BreadcrumbsData;
