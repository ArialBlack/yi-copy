import ImageWithTextTwig from './image-with-text.twig';
import ImageWithTextData from './image-with-text.yml';
import ImageWithTextDocs from './image-with-text.mdx';

const args = {
  title: 'Organisms/CTA Block/Image With Text',
  parameters: { 
    docs: { 
      page: ImageWithTextDocs
    } 
  },
  argTypes: {
    button_text_color: {
      name: 'Button Text Color',
      defaultValue: '#38b2ac',
      control: {
        type: 'select',
        options: {
          'White': '#fff',
          'Black': '#000',
          'Primary': '#38b2ac',
          'Secondary': '#a0aec0',
          'Accent': '#4a5568',
          'Highlight': '#90cdf4',
          'Warning': '#faf089',
          'Error': '#feb2b2',
          'Message': '#9ae6b4'
        },
      },
      table: {
        category: 'Settings',
        subcategory: 'Buttons'
      },
    },
    image_position: {
      name: 'Image Position',
      description: '*parameter*: `Image Position`',
      defaultValue: "right",
      control: {
        type: 'select',
        options: ["left", "right"],
      },
      table: {
        category: 'Styling Options',
        type: {
          summary: 'enum',
          detail: 'image position'
        },
        defaultValue: {
          summary: 'right'
        },
      },
    },
    image_fit: {
      name: 'Image Fit',
      description: '*parameter*: `Image Fit`',
      defaultValue: "cover",
      control: {
        type: 'select',
        options: ["cover", "contain", "fill", "none"],
      },
      table: {
        category: 'Styling Options',
        type: {
          summary: 'enum',
          detail: 'image fit'
        },
        defaultValue: {
          summary: 'cover'
        },
      },
    },
    config_term: {
      table: {
        disable: true
      },
    },
    heading: {
      table: {
        disable: true
      },
    },
    body: {
      table: {
        disable: true
      },
    },
    link: {
      table: {
        disable: true
      },
    },
    image: {
      table: {
        disable: true
      },
    },
  }
};

export default args;

export const Primary = (args) => (
  ImageWithTextTwig(args)
);

Primary.args = ImageWithTextData;
