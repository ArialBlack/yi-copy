// Component attachment to Storybook

/* If your component needs AlpineJS. */
// import Alpine from 'alpinejs';

/* If your component needs Drupal attributes. */
// import drupalAttribute from 'drupal-attribute'

import HeaderWithUtilitySimpleTwig from './header-with-utility-simple.twig';
import HeaderWithUtilitySimpleData from './header-with-utility-simple.yml';
import HeaderWithUtilitySimpleDocs from './header-with-utility-simple.mdx';

let args = {
  title: 'Organisms/Header/Header With Utility Simple',
  parameters: { 
    docs: { 
      page: HeaderWithUtilitySimpleDocs
    } 
  },
  argTypes: {
    config_term: {
      table: {
        disable: true
      },
    },
    site_logo: {
      table: {
        disable: true
      },
    },
    site_logo_mobile: {
      table: {
        disable: true
      },
    },
    form: {
      table: {
        disable: true
      },
    },
    form_mobile: {
      table: {
        disable: true
      },
    },
    menu: {
      table: {
        disable: true
      },
    },
    mobile_button_svg: {
      table: {
        disable: true
      },
    },
    mobile_close_svg: {
      table: {
        disable: true
      },
    },
  }
}

export default args;

export const Primary = (args) => (
  HeaderWithUtilitySimpleTwig(args)
);

Primary.args = HeaderWithUtilitySimpleData;
