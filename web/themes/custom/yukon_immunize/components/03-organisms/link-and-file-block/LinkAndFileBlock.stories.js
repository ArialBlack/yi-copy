// Component attachment to Storybook

/* If your component needs AlpineJS. */
// import Alpine from 'alpinejs';

/* If your component needs Drupal attributes. */
// import drupalAttribute from 'drupal-attribute'

import LinkAndFileBlockTwig from './link-and-file-block.twig';
import LinkAndFileBlockData from './link-and-file-block.yml';
import LinkAndFileBlockDocs from './link-and-file-block.mdx';

const args = {
  title: 'Organisms/Resources/Link and File Block',
  parameters: { 
    docs: { 
      page: LinkAndFileBlockDocs
    } 
  },
  argTypes: {
    link_color: {
      name: 'Link Color',
      defaultValue: 'black',
      control: {
        type: 'select',
        options: {
          White: 'white',
          Black: 'black',
          Translucent: 'translucent',
          Primary: 'primary',
          PrimaryLight: 'primaryLight',
          Secondary: 'secondary',
          Accent: 'accent',
          Text: 'text',
          Background: 'background',
          Highlight: 'highlight',
          Warning: 'warning',
          Error: 'error',
          Message: 'message'
        }
      },
      table: {
        category: 'Settings',
      },
    },
    config_term: {
      table: {
        disable: true
      },
    },
    links: {
      table: {
        disable: true
      },
    },
    link_color: {
      table: {
        disable: true
      },
    },
    file: {
      table: {
        disable: true
      },
    },
  }
};

export default args;

export const Primary = (args) => (
  LinkAndFileBlockTwig(args)
);

Primary.args = LinkAndFileBlockData;
