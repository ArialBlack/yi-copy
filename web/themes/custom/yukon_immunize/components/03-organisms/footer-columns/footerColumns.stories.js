import FooterColumnsTwig from './footer-columns.twig';
import FooterColumnsData from './footer-columns.yml';
import FooterColumnsCode from '!raw-loader!./footer-columns.twig'
import FooterColumnsDocs from './footer-columns.mdx'

const args = {
  title: 'Organisms/Footer/Footer Columns',
  parameters: {
    componentSubtitle: 'Customizable Footer',
    docs: {
      source: {
        code: FooterColumnsCode
      },
      page: FooterColumnsDocs
    },
  },
  argTypes: {
    copyright_text: {
      name: "Copyright Text",
      table: {
        category: 'Copyright Text'
      },
    },
    config_term: {
      table: {
        disable: true
      },
    },
    menus: {
      table: {
        disable: true
      },
    },
    social_links: {
      table: {
        disable: true
      },
    },
    footer_logo: {
      table: {
        disable: true
      },
    },
  },
};

export default args;

export const Primary = (args) => (FooterColumnsTwig(args));

Primary.args = FooterColumnsData;
