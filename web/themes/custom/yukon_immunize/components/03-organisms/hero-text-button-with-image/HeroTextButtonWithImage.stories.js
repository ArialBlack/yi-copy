import HeroTextButtonWithImageTwig from './hero-text-button-with-image.twig';
import HeroTextButtonWithImageData from './hero-text-button-with-image.yml';
import HeroTextButtonWithImageDocs from './hero-text-button-with-image.mdx';

const args = {
  title: 'Organisms/Hero/Hero Text Button With Image',
  parameters: { 
    docs: { 
      page: HeroTextButtonWithImageDocs
    } 
  },
  argTypes: {
    background_color: {
      name: 'Background Color',
      description: '*parameter*: `Background Color`',
      defaultValue: "default",
      control: {
        type: 'select',
        options: ["default", "text", "background", "primary", "secondary", "accent", "highlight", "warning", "error", "message"],
      },
      table: {
        category: 'Styling Options',
        type: {
          summary: 'enum',
          detail: 'hero background color'
        },
        defaultValue: {
          summary: 'default'
        },
      },
    },
    heading_color: {
      name: 'Heading Color',
      description: '*parameter*: `Heading Color`',
      defaultValue: "default",
      control: {
        type: 'select',
        options: ["default", "text", "background", "primary", "secondary", "accent", "highlight", "warning", "error", "message"],
      },
      table: {
        category: 'Styling Options',
        type: {
          summary: 'enum',
          detail: 'color of content heading'
        },
        defaultValue: {
          summary: 'default'
        },
      },
    },
    body_color: {
      name: 'Body Color',
      description: '*parameter*: `Body Color`',
      defaultValue: "default",
      control: {
        type: 'select',
        options: ["default", "text", "background", "primary", "secondary", "accent", "highlight", "warning", "error", "message"],
      },
      table: {
        category: 'Styling Options',
        type: {
          summary: 'enum',
          detail: 'color of content body'
        },
        defaultValue: {
          summary: 'default'
        },
      },
    },
    button_text_color: {
      name: 'Button Text Color',
      defaultValue: '#fff',
      control: {
        type: 'select',
        options: {
          'White': '#fff',
          'Black': '#000',
          'Primary': '#38b2ac',
          'Secondary': '#a0aec0',
          'Accent': '#4a5568',
          'Highlight': '#90cdf4',
          'Warning': '#faf089',
          'Error': '#feb2b2',
          'Message': '#9ae6b4'
        },
      },
      table: {
        category: 'Settings',
        subcategory: 'Buttons'
      },
    },
    button_bg_color: {
      name: 'Button Background Color',
      defaultValue: '#38b2ac',
      control: {
        type: 'select',
        options: {
          'White': '#fff',
          'Black': '#000',
          'Primary': '#38b2ac',
          'Secondary': '#a0aec0',
          'Accent': '#4a5568',
          'Highlight': '#90cdf4',
          'Warning': '#faf089',
          'Error': '#feb2b2',
          'Message': '#9ae6b4'
        },
      },
      table: {
        category: 'Settings',
        subcategory: 'Buttons'
      },
    },
    button_text_color_hover: {
      name: 'Button Text Color (Hover/Focus)',
      defaultValue: '#fff',
      control: {
        type: 'select',
        options: {
          'White': '#fff',
          'Black': '#000',
          'Primary': '#38b2ac',
          'Secondary': '#a0aec0',
          'Accent': '#4a5568',
          'Highlight': '#90cdf4',
          'Warning': '#faf089',
          'Error': '#feb2b2',
          'Message': '#9ae6b4'
        },
      },
      table: {
        category: 'Settings',
        subcategory: 'Buttons'
      },
    },
    button_bg_color_hover: {
      name: 'Button Background Color (Hover/Focus)',
      defaultValue: '#38b2ac',
      control: {
        type: 'select',
        options: {
          'White': '#fff',
          'Black': '#000',
          'Primary': '#38b2ac',
          'Secondary': '#a0aec0',
          'Accent': '#4a5568',
          'Highlight': '#90cdf4',
          'Warning': '#faf089',
          'Error': '#feb2b2',
          'Message': '#9ae6b4'
        },
      },
      table: {
        category: 'Settings',
        subcategory: 'Buttons'
      },
    },
    config_term: {
      table: {
        disable: true
      },
    },
    heading: {
      table: {
        disable: true
      },
    },
    body: {
      table: {
        disable: true
      },
    },
    links: {
      table: {
        disable: true
      },
    },
    image: {
      table: {
        disable: true
      },
    },
  }
};

export default args;

export const Primary = (args) => (
  HeroTextButtonWithImageTwig(args)
);

Primary.args = HeroTextButtonWithImageData;
