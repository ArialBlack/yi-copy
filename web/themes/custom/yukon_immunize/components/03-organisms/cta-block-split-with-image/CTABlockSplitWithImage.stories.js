import CTABlockSplitWithImageTwig from './cta-block-split-with-image.twig';
import CTABlockSplitWithImageData from './cta-block-split-with-image.yml';
import CTABlockSplitWithImageDocs from './cta-block-split-with-image.mdx';

let args = {
  title: 'Organisms/CTA Block/CTA Block Split With Image',
  parameters: { 
    docs: { 
      page: CTABlockSplitWithImageDocs
    } 
  },
  argTypes: {
    image_align: {
      name: 'Image Align',
      defaultValue: 'Left',
      control: {
        type: 'inline-radio',
        options: ['Left', 'Right']
      },
      table: {
        category: 'Settings'
      },
    },
    bg_color: {
      name: 'Background Color',
      defaultValue: 'accent',
      control: {
        type: 'select',
        options: {
          White: 'white',
          Black: 'black',
          Translucent: 'translucent',
          Primary: 'primary',
          PrimaryLight: 'primaryLight',
          Secondary: 'secondary',
          Accent: 'accent',
          Text: 'text',
          Background: 'background',
          Highlight: 'highlight',
          Warning: 'warning',
          Error: 'error',
          Message: 'message'
        }
      },
      table: {
        category: 'Settings',
        subcategory: 'Content Section'
      },
    },
    intro_color: {
      name: 'Intro Text Color',
      defaultValue: 'white',
      control: {
        type: 'select',
        options: {
          White: 'white',
          Black: 'black',
          Translucent: 'translucent',
          Primary: 'primary',
          PrimaryLight: 'primaryLight',
          Secondary: 'secondary',
          Accent: 'accent',
          Text: 'text',
          Background: 'background',
          Highlight: 'highlight',
          Warning: 'warning',
          Error: 'error',
          Message: 'message'
        }
      },
      table: {
        category: 'Settings',
        subcategory: 'Content Section'
      },
    },
    title_color: {
      name: 'Title Text Color',
      defaultValue: 'white',
      control: {
        type: 'select',
        options: {
          White: 'white',
          Black: 'black',
          Translucent: 'translucent',
          Primary: 'primary',
          PrimaryLight: 'primaryLight',
          Secondary: 'secondary',
          Accent: 'accent',
          Text: 'text',
          Background: 'background',
          Highlight: 'highlight',
          Warning: 'warning',
          Error: 'error',
          Message: 'message'
        }
      },
      table: {
        category: 'Settings',
        subcategory: 'Content Section'
      },
    },
    text_color: {
      name: 'Text Color',
      defaultValue: 'white',
      control: {
        type: 'select',
        options: {
          White: 'white',
          Black: 'black',
          Translucent: 'translucent',
          Primary: 'primary',
          PrimaryLight: 'primaryLight',
          Secondary: 'secondary',
          Accent: 'accent',
          Text: 'text',
          Background: 'background',
          Highlight: 'highlight',
          Warning: 'warning',
          Error: 'error',
          Message: 'message'
        }
      },
      table: {
        category: 'Settings',
        subcategory: 'Content Section'
      },
    },
    link_color: {
      name: 'Link Text Color',
      defaultValue: '#000',
      control: {
        type: 'select',
        options: {
          'White': '#fff',
          'Black': '#000',
          'Primary': '#38b2ac',
          'Secondary': '#a0aec0',
          'Accent': '#4a5568',
          'Highlight': '#90cdf4',
          'Warning': '#faf089',
          'Error': '#feb2b2',
          'Message': '#9ae6b4'
        },
      },
      table: {
        category: 'Settings',
        subcategory: 'Content Section'
      },
    },
    link_color_hover: {
      name: 'Link Text Color (Hover / Focus)',
      defaultValue: '#38b2ac',
      control: {
        type: 'select',
        options: {
          'White': '#fff',
          'Black': '#000',
          'Primary': '#38b2ac',
          'Secondary': '#a0aec0',
          'Accent': '#4a5568',
          'Highlight': '#90cdf4',
          'Warning': '#faf089',
          'Error': '#feb2b2',
          'Message': '#9ae6b4'
        },
      },
      table: {
        category: 'Settings',
        subcategory: 'Content Section'
      },
    },
    link_bg_color: {
      name: 'Link Background Color',
      defaultValue: '#fff',
      control: {
        type: 'select',
        options: {
          'White': '#fff',
          'Black': '#000',
          'Primary': '#38b2ac',
          'Secondary': '#a0aec0',
          'Accent': '#4a5568',
          'Highlight': '#90cdf4',
          'Warning': '#faf089',
          'Error': '#feb2b2',
          'Message': '#9ae6b4'
        },
      },
      table: {
        category: 'Settings',
        subcategory: 'Content Section'
      },
    },
    link_bg_color_hover: {
      name: 'Link Background Color (Hover / Focus)',
      defaultValue: '#fff',
      control: {
        type: 'select',
        options: {
          'White': '#fff',
          'Black': '#000',
          'Primary': '#38b2ac',
          'Secondary': '#a0aec0',
          'Accent': '#4a5568',
          'Highlight': '#90cdf4',
          'Warning': '#faf089',
          'Error': '#feb2b2',
          'Message': '#9ae6b4'
        },
      },
      table: {
        category: 'Settings',
        subcategory: 'Content Section'
      },
    },
    config_term: {
      table: {
        disable: true
      },
    },
    intro: {
      table: {
        disable: true
      },
    },
    heading: {
      table: {
        disable: true
      },
    },
    body: {
      table: {
        disable: true
      },
    },
    link_url: {
      table: {
        disable: true
      },
    },
    link_text: {
      table: {
        disable: true
      },
    },
    image: {
      table: {
        disable: true
      },
    },
  }
}

export default args;

export const Primary = (args) => (
  CTABlockSplitWithImageTwig(args)
);

Primary.args = CTABlockSplitWithImageData;
