// Component attachment to Storybook

/* If your component needs AlpineJS. */
// import Alpine from 'alpinejs';

/* If your component needs Drupal attributes. */
// import drupalAttribute from 'drupal-attribute'

import ContactUsSimpleTwig from './contact-us-simple.twig';
import ContactUsSimpleData from './contact-us-simple.yml';
import ContactUsSimpleDocs from './contact-us-simple.mdx';

let args = {
  title: 'Organisms/Form/Contact Us Simple',
  parameters: { 
    docs: { 
      page: ContactUsSimpleDocs
    } 
  },
  argTypes: {
    button_text_color: {
      name: 'Submit Button Text Color',
      defaultValue: '#fff',
      control: {
        type: 'select',
        options: {
          'White': '#fff',
          'Black': '#000',
          'Primary': '#38b2ac',
          'Secondary': '#a0aec0',
          'Accent': '#4a5568',
          'Highlight': '#90cdf4',
          'Warning': '#faf089',
          'Error': '#feb2b2',
          'Message': '#9ae6b4'
        },
      },
      table: {
        category: 'Settings'
      },
    },
    button_color: {
      name: 'Submit Button Color',
      defaultValue: '#38b2ac',
      control: {
        type: 'select',
        options: {
          'White': '#fff',
          'Black': '#000',
          'Primary': '#38b2ac',
          'Secondary': '#a0aec0',
          'Accent': '#4a5568',
          'Highlight': '#90cdf4',
          'Warning': '#faf089',
          'Error': '#feb2b2',
          'Message': '#9ae6b4'
        },
      },
      table: {
        category: 'Settings'
      },
    },
    button_color_hover: {
      name: 'Submit Button Color (Hover / Focus)',
      defaultValue: '#319795',
      control: {
        type: 'select',
        options: {
          'White': '#fff',
          'Black': '#000',
          'Primary': '#38b2ac',
          'Primary Hover': '#319795',
          'Secondary': '#a0aec0',
          'Accent': '#4a5568',
          'Highlight': '#90cdf4',
          'Warning': '#faf089',
          'Error': '#feb2b2',
          'Message': '#9ae6b4'
        },
      },
      table: {
        category: 'Settings'
      },
    },
    config_term: {
      table: {
        disable: true
      },
    },
    form: {
      table: {
        disable: true
      },
    },
    form_heading: {
      table: {
        disable: true
      },
    },
    form_intro: {
      table: {
        disable: true
      },
    },
  }
};

export default args;

export const Primary = (args) => (
  ContactUsSimpleTwig(args)
);

Primary.args = ContactUsSimpleData;
