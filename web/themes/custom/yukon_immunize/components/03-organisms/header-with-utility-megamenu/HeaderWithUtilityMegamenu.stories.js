// Component attachment to Storybook

/* If your component needs AlpineJS. */
// import Alpine from 'alpinejs';

/* If your component needs Drupal attributes. */
// import drupalAttribute from 'drupal-attribute'

import HeaderWithUtilityMegamenuTwig from './header-with-utility-megamenu.twig';
import HeaderWithUtilityMegamenuData from './header-with-utility-megamenu.yml';
import HeaderWithUtilityMegamenuDocs from './header-with-utility-megamenu.mdx';

let args = {
  title: 'Organisms/Header/Header with Utility Megamenu',
  parameters: { 
    docs: { 
      page: HeaderWithUtilityMegamenuDocs
    } 
  },
  argTypes: {
    config_term: {
      table: {
        disable: true
      },
    },
    site_logo: {
      table: {
        disable: true
      },
    },
    site_logo_mobile: {
      table: {
        disable: true
      },
    },
    form: {
      table: {
        disable: true
      },
    },
    form_mobile: {
      table: {
        disable: true
      },
    },
    menu: {
      table: {
        disable: true
      },
    },
    mobile_button_svg: {
      table: {
        disable: true
      },
    },
    mobile_close_svg: {
      table: {
        disable: true
      },
    },
  }
}

export default args;

export const Primary = (args) => (
  HeaderWithUtilityMegamenuTwig(args)
);

Primary.args = HeaderWithUtilityMegamenuData;
