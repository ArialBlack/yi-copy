import Glide from '@glidejs/glide';

export default function ({
  startAt = 0,
  autoplay = 0,
  hoverpause = true,
  keyboard = true,
  swipeThreshold = 80,
  dragThreshold = 120,
  perTouch = false,
  touchRatio = 0.5,
  touchAngle = 45,
  animationDuration = 400,
  animationTimingFunc = 'cubic-bezier(0.165, 0.840, 0.440, 1.000)',
  direction = 'ltr',
  peek = 0,
  breakpoints = {},
  throttle = 25,
}) {
  const setBulletPosition = function() {
    let content = document.querySelector('.block--carousel-with-full-height-content .glide__slide--active .glide__slide--content');
    let activeSlide = document.querySelector('.block--carousel-with-full-height-content .glide__slide--active');
    let bullets = document.querySelector('.block--carousel-with-full-height-content .glide__bullets');
    let contentPosition = content.getBoundingClientRect();

    let xOffset = 0;

    if (window.innerWidth >= 1024) {
      bullets.style.top = (content.offsetHeight - 50) + "px";
      xOffset = 72;
    }
    else if (window.innerWidth >= 768) {
      bullets.style.top = (activeSlide.offsetHeight - 40) + "px";
      xOffset = 52;
    }
    else {
      bullets.style.top = (activeSlide.offsetHeight - 40) + "px";
      xOffset = 20;
    }

    bullets.style.left = (contentPosition.left + xOffset) + "px";
  }

  const carousel = new Glide("#carousel-with-full-height-content", {
    type: "carousel",
    startAt: startAt,
    autoplay: autoplay,
    hoverpause: hoverpause,
    keyboard: keyboard,
    swipeThreshold: swipeThreshold,
    dragThreshold: dragThreshold,
    perTouch: perTouch,
    touchRatio: touchRatio,
    touchAngle: touchAngle,
    animationDuration: animationDuration,
    animationTimingFunc: animationTimingFunc,
    peek: peek,
    breakpoints: breakpoints,
    throttle: throttle
  });
  
  document.querySelector('.block--carousel-with-full-height-content .glide__slides').classList.add('flex');

  let slides = [];
  let allSlides = [];

  carousel.on('mount.after', function() {
    slides = document.querySelectorAll('.block--carousel-with-full-height-content .glide__slide:not(.glide__slide--clone) .carousel-with-full-height-content__button');
    allSlides = document.querySelectorAll('.block--carousel-with-full-height-content .glide__slide .carousel-with-full-height-content__button');

    for (let i = 0; i < allSlides.length; i++) {
      allSlides[i].tabIndex = -1;
    }
    slides[startAt].tabIndex = 0;
  });

  carousel.on('run', function() {
    slides[carousel.index].tabIndex = 0;
  });

  carousel.on('move.after', function() {
    for (let i = 0; i < slides.length; i++) {
      if (i != carousel.index) {
        slides[i].tabIndex = -1;
      }
    }
  });

  carousel.mount();
  setBulletPosition();
};
