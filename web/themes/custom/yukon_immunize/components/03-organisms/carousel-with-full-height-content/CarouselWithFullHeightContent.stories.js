import CarouselWithFullHeightContentTwig from './carousel-with-full-height-content.twig';
import CarouselWithFullHeightContentData from './carousel-with-full-height-content.yml';
import CarouselWithFullHeightContentDocs from './carousel-with-full-height-content.mdx';
import callback from './GlideInitForStorybook.js';

const renderStroyWithCallback = (component, callback, options, timeout) => {
  if (callback) 
    setTimeout(() => callback(options), timeout)
  return component;
}

const args = {
  title: 'Organisms/Carousel/Carousel with Full Height Content',
  parameters: { 
    docs: { 
      page: CarouselWithFullHeightContentDocs
    } 
  },
  argTypes: {
    config_term: {
      table: {
        disable: true
      },
    },
    slides: {
      table: {
        disable: true
      },
    },
    images: {
      table: {
        disable: true
      },
    },
    storybook: {
      table: {
        disable: true
      },
    },
  }
};

export default args;

export const Primary = (args) => {
  return (
    renderStroyWithCallback(
      CarouselWithFullHeightContentTwig(args),
      callback,
      args,
      50
    )
  )
};

Primary.args = CarouselWithFullHeightContentData;
