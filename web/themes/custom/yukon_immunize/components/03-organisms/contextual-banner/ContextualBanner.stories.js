// Component attachment to Storybook

/* If your component needs AlpineJS. */
// import Alpine from 'alpinejs';

/* If your component needs Drupal attributes. */
// import drupalAttribute from 'drupal-attribute'

import ContextualBannerTwig from './contextual-banner.twig';
import ContextualBannerData from './contextual-banner.yml';
import ContextualBannerDocs from './contextual-banner.mdx';

export default {
  title: 'Organisms/Banners/Contextual Banner',
  parameters: { 
    docs: { 
      page: ContextualBannerDocs
    } 
  },
};

export const ContextualBanner = () => (
  ContextualBannerTwig(ContextualBannerData)
);
