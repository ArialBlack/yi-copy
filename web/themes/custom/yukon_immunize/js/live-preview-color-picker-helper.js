(function($, Drupal, drupalSettings){
  'use strict';

  Drupal.behaviors.live_preview_color_picker_helper = {
    attach: function(context) {
      Drupal.live_preview_color_picker_helper.colorHelper();
    }
  };

  Drupal.live_preview_color_picker_helper = {
    colorHelper: function () {
      if (window.location.href.indexOf('/layout') > -1) {
        let colourFields = document.querySelectorAll('.color-field-widget-box-form');

        for (let i = 0; i < colourFields.length; i++) {
          setTimeout(function() {
            let children = colourFields[i].children;
  
            for (let j = 0; j < children.length; j++) {
              children[j].addEventListener('click', function() {
                jQuery('.live-preview-live-preview').mousedown();
              });
            }
          }, 0);
        }
      }
    }
  };
})(jQuery, Drupal, drupalSettings);