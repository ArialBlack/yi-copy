(function ($) {
  Drupal.behaviors.yiShiftedTextSection = {
    attach: function (context, settings) {
      $('.shifted-text-section', context).once('yiShiftedTextSection').each(function () {
        let $block = $(this);
        let $previousSection = $block.closest('.layout').prev();

        $previousSection.css({
          'position': 'relative',
          'z-index': '1'
        });

        $block.css('z-index', '0');
      });
    }
  };
}(jQuery));
