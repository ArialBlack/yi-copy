// https://drupal.stackexchange.com/questions/200498/hide-the-term-id-on-autocomplete-widget/295790#295790
(function($, Drupal, drupalSettings){
  'use strict';

  Drupal.behaviors.sectionGradient = {
    attach: function(context) {
      Drupal.sectionGradient.applyGradient();
    }
  };

  Drupal.sectionGradient = {
    applyGradient: function () {
      let sections = document.querySelectorAll('.layout');

      for (let i = 0; i < sections.length; i++) {
        let gradientPercentage = null;
        let backgroundColor = null;
        let backgroundSide = null;

        if (!sections[i].classList.contains('section-no-bg-gradient')) {
          for (let j = 0; j < sections[i].classList.length; j++) {
            if (sections[i].classList[j].includes('section-bg-gradient-')) {
              let gradientInfo = sections[i].classList[j].replace('section-bg-gradient-', '').split('-');
              gradientPercentage = gradientInfo[0];
              backgroundSide = gradientInfo[1];
            }
  
            if (sections[i].classList[j].includes('bg-')) {
              backgroundColor = sections[i].classList[j].replace('bg-', '');
            }
          }
  
          if (gradientPercentage != null && backgroundColor != null && backgroundSide != null) {
            let degrees = null;
            let sectionHeight = null;
            let regionHeightLeft = null;
            
            if (window.innerWidth >= 1024) {
              degrees = '90';
            }
            else {
              degrees = '180';
              sectionHeight = sections[i].offsetHeight;
              regionHeightLeft = sections[i].children[0].children[0].offsetHeight;
            }

            if (sectionHeight != null) {
              gradientPercentage = ((regionHeightLeft / sectionHeight) * 100).toFixed(2);
            }

            if (backgroundSide == 'right') {
              sections[i].style.background = `linear-gradient(${degrees}deg, #FFF ${gradientPercentage}%, var(--${backgroundColor}) ${gradientPercentage}%)`;
            }
            else {
              sections[i].style.background = `linear-gradient(${degrees}deg, var(--${backgroundColor}) ${gradientPercentage}%, #FFF ${gradientPercentage}%)`;
            }
          }
        }
      }
    }
  };
})(jQuery, Drupal, drupalSettings);