// https://drupal.stackexchange.com/questions/200498/hide-the-term-id-on-autocomplete-widget/295790#295790
(function($, Drupal, drupalSettings){
  'use strict';

  Drupal.behaviors.sectionPaddingMargin = {
    attach: function(context) {
      Drupal.sectionPaddingMargin.applySpacing();
    }
  };

  Drupal.sectionPaddingMargin = {
    calcSpacing: function(directions) {
      let splitDirections = directions.split('-');
      let directionCombo = splitDirections[1]; // example: all, ltr, x
      let spacing = Number(splitDirections[2]); // amount of pixels
      let sTop = 0, sBottom = 0, sLeft = 0, sRight = 0;

      switch (directionCombo) {
        case 'all':
          sTop = sBottom = sRight = sLeft = spacing;
          break;
        case 'x':
          sRight = sLeft = spacing;
          break;
        case 'y':
          sTop = sBottom = spacing;
          break;
        default:
          for (let i = 0; i < directionCombo.length; i++) {
            switch (directionCombo.charAt(i)) {
              case 't':
                sTop = spacing;
                break;
              case 'b':
                sBottom = spacing;
                break;
              case 'l':
                sLeft = spacing;
                break;
              case 'r':
                sRight = spacing;
                break;
            }
          }   
      }

      return [sTop, sRight, sBottom, sLeft];
    },
    applySpacing: function () {
      let sections = document.querySelectorAll('.layout, .layout__region');

      for (let i = 0; i < sections.length; i++) {
        let padding = [0, 0, 0, 0], margin = [0, 0, 0, 0];
        let applyPadding = false, applyMargin = false;

        for (let j = 0; j < sections[i].classList.length; j++) {
          if (/pd-.*/.test(sections[i].classList[j])) {
            applyPadding = true;

            let spacing = this.calcSpacing(sections[i].classList[j]);
            for (let k = 0; k < padding.length; k++) {
              padding[k] = (padding[k] > 0 ? padding[k] + spacing[k] : spacing[k]);
            }
          }
          else if (/mr-.*/.test(sections[i].classList[j])) {
            applyMargin = true;
            
            let spacing = this.calcSpacing(sections[i].classList[j]);
            for (let k = 0; k < margin.length; k++) {
              margin[k] = (margin[k] > 0 ? margin[k] + spacing[k] : spacing[k]);
            }
          }
        }

        if (applyPadding) {
          sections[i].style.padding = `${padding[0]}px ${padding[1]}px ${padding[2]}px ${padding[3]}px`;
        }
        
        if (applyMargin && !sections[i].classList.contains('mx-auto')) {
          sections[i].style.margin = `${margin[0]}px ${margin[1]}px ${margin[2]}px ${margin[3]}px`;
        }
        else if (applyMargin && sections[i].classList.contains('mx-auto')) {
          sections[i].style.margin = `${margin[0]}px auto ${margin[2]}px auto`;
        }
      }
    }
  };
})(jQuery, Drupal, drupalSettings);