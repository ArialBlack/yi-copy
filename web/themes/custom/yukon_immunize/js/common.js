(function ($) {
  Drupal.behaviors.yiCommon = {
    attach: function (context, settings) {
      $('.cke-style-link a', context).once('yiCommonCKELink').each(function () {
        let $link = $(this);
        let colorStyle = $link.find('span').css('color');

        if (colorStyle) {
          $link.css('color', colorStyle);
        }
      });

      $('.yi-gmap--contacts', context).once('yiGmap').each(function () {
        $(window).on('load', function () {
          $('div[role="button"]').click();
        });
      });
    }
  };
}(jQuery));

