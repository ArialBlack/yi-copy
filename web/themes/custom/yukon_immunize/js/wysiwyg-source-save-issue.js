// https://www.drupal.org/project/drupal/issues/3095304#comment-14029908
(function($, Drupal, drupalSettings){
  'use strict';

  Drupal.behaviors.wysiwyg_save_issue = {
    attach: function(context) {
      Drupal.wysiwyg_save_issue.save_wysiwyg();
    }
  };

  Drupal.wysiwyg_save_issue = {
    save_wysiwyg: function() {
      var origBeforeSubmit = Drupal.Ajax.prototype.beforeSubmit;
      Drupal.Ajax.prototype.beforeSubmit = function (formValues, element, options) {
        if (typeof(CKEDITOR) !== 'undefined' && CKEDITOR.instances) {
          const instances = Object.values(CKEDITOR.instances);
          instances.forEach(editor => {
            formValues.forEach(formField => {
              // Get field name from the id in the editor so that it covers all
              // fields using ckeditor.
              let element = document.querySelector(`#${editor.name}`)
              if (element) {
                let fieldName = element.getAttribute('name');
                if (formField.name === fieldName && editor.mode === 'source') {
                  formField.value = editor.getData();
                }
              }
            });
          });
        }
        return origBeforeSubmit.apply(this, arguments);
      };
    }
  }
})(jQuery, Drupal, drupalSettings);