(function ($) {
  Drupal.behaviors.yiSelect2 = {
    attach: function (context, settings) {
      $('select.select2', context).once('yiSelect2').each(function (e) {
        let $select = $(this);
        $select.select2({
          'minimumResultsForSearch': 15
        });

        $select.on("select2:select", function (e) {
          if (e.params.data.element.getAttribute('data-url')) {
            window.open(e.params.data.element.getAttribute('data-url'));
          }
        });
      });
    }
  };
}(jQuery));

