(function ($) {
  Drupal.behaviors.yiResponsiveTable = {
    attach: function (context, settings) {
      $('.block--immunization-schedules-tables table', context).once('yiResponsiveTable').each(function () {
        $(this).basictable({
          breakpoint: 768,
        });
      });

      $('.immunization-schedules-table--children .bt-content p', context).once('isTableChildrenCell').each(function () {
        let $cellContent = $(this);

        switch ($cellContent.text()) {
          case '-':
            $(this).closest('td').addClass('cell cell--no');
            break;
          case 'v':
            $(this).closest('td').addClass('cell cell--yes');
            break;
          case 'v*':
            $(this).closest('td').addClass('cell cell--yes-1');
            break;
          case 'v**':
            $(this).closest('td').addClass('cell cell--yes-2');
            break;
          case 'v***':
            $(this).closest('td').addClass('cell cell--yes-3');
            break;
        }
      });
    }
  };
}(jQuery));

