# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.5.0] - 2021-11-29
### Added
- Added Tabs General component.
- Added svg extention for footer logo uploads.

### Changed
- Removed Document Link Label field from Image with Text Doc Links and replaced with new plain text field (limit 1).
- Changed body field in Image with Text Doc Links to use Minimum Format text format.
- Removed long plain text from Image with Text and replaced with WYSIWYG.
- Updated Video Gallery Grid component.

## [1.4.1] - 2021-11-15
### Changed 
- Changed Header with Utility Megamenu component installation guide in .mdx file

## [1.4.0] - 2021-11-1
### Added 
- Added a checkbox to enable two columns display for contents/answers in faq-block component
- Created new Alerts Carousel component.

### Changed 
- Changed field_body to unlimited in the paragraph in faq-block component

## [1.3.0] - 2021-10-27
### Added 
- Updated New Related Events component to be Contextual View and not Manual Selection
- Removed header and footer from login components

## [1.2.0] - 2021-10-15
### Added 
- Added global styling for maintenance page
- Fixed A-Z listing display

## [1.1.4] - 2021-10-14
### Added 
- Added Carousel With Full Height Content from SY
- Added CTA with Hierarchy Links, Image With Text And Doc Links, Link and File Block, Next Node Manual and Resources Dropdown from SY. Added images of each component and added the components to yukon_immunize and SB.
- Added Missing Images for blocks in Layout Browser.
- Added global theming for revisions.
- Added Card Grid with Featured Card from SY.
- Added Milestone Scrollbar from SY.
### Changed 
- Fixed pdf file is open with same tab, now pdf can be opened with a new tab.
- Fixed bug with Login 2 to display Username instead of Email Address.
- Fixed linkit modal incorrecly display issue.
- Fixed yml and stories.js files for Milestone Scrollbar.
- Fixed Mega Menu Dropdown Issues.
- Compare Revisions needs Global Theming. 
- PDF Must Open in a New Window By Default.
- Username and password fields didn't render on template login 2 and 3.
- Fixed Newsletter signup blocks are not linked correctly.
- Clearned Leftover Menu Configuration.
- Fixed CTA block has reference to body.
- Fixed Various storybook bugs from different components.
- Fixed Block link background colour not working.

## [1.1.3] - 2021-09-23
### Added
- Added Header with Utility Megamenu organism and Navigation Megamenu Links molecule from WT (2021-09-23).
- Added Big Image Card component from SOS, simpliefied  a few twig files (2021-09-22)
- Added Header with Utility Simple organism and Navigation Hover Links, Search Fullscreen and Utility Simple Links molecules. (2021-09-22)
- Added Card with Description Link or Date from SOS (2021-09-21)
- Added Card with Title Link or Date from SOS and updated cta-with-body-only (2021-09-20)
- Added Pointer and Icon Goals component from SOS (2021-09-17)
- Added Full Width Component With Icons component from SOS (2021-09-16)
- Added CTA with Body Only component from SOS, fixed storybook for Quick Links and updated heading with button config (2021-09-15)
- Added Quick Links Component from SOS (2021-09-14)
- Added responsive image style to card grid simple (2021-09-07)
### Changed
- Updated all field names for the components of Yukon Water Board. (2021-09-13)

## [1.1.2] - 2021-08-24
### Changed
- Fixed section-margin-padding.js to apply to regions as well and made styling unapply on 1024px and lower

## [1.1.1] - 2021-08-24
### Changed
- New media block component
- New media block component
- Adding if statements to FAQ twigs for subtext
- Fixing carousel if statement to run or not
- Updated config_term for carousel_block
- fixing .yukon_immunize.yml with new names
- Both stat component paragraph conversion
- Footer conversion paragraphs
- FAQ Side by Side paragraph conversion
- FAQ Column paragraph conversion
- FAQ Block Conversion
- Carousel Text Button paragraph conversion
- Card Grid Simple paragraph conversion
- Changing auto year and adding rel=noopener as per Davids suggestions
- Feature to split background colour on two column section
- Feature to contain content but have a section go full width
- CSS for colour boxes
- Modified molecules with new atom change
- Modified atoms to be simpler
- Added Hex To Twig requirements for Yukon Water Board
- Added a hidden advanced options that can be shown by clicking advanced options

## [1.1.0] - 2021-08-03
### New Version
- Start of version 1.1.0.

## [1.0.5] - 2021-07-12
### Changed
- Changed padding breakpoint in Layout Section options to lg rather than md.
- Fixed section responsive styling in `components/layout-section.css`.

## [1.0.4] - 2021-06-29
### Changed
- Fixed JS issue where entity reference autocomplete fields would fail in LB.
- Added responsive image style to Carousel Text Button.
- Fixed Content Position for Carousel Text Button.

## [1.0.3] - 2021-06-23
### Changed
- Fixed a bug with Accordian-simple and FAQ-Accordian to allow for tabindex

## [1.0.2] - 2021-06-23
### Added
- Adding JS fix to carousels to prevent page crash when adding to layout builder.
- Added image styles to carousels and fixed styling.

## [1.0.1] - 2021-06-18
### Added
- Added a changelog to track all updates made to yukon_immunize.