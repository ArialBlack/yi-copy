// tailwind.config.js
const { colors } = require('tailwindcss/defaultTheme')
// const plugin = require('tailwindcss/plugin')

module.exports = {
  experimental: {
    applyComplexClasses: true,
  },
  future: {},
  purge: {
    content: [
      'templates/*.twig',
      'templates/**/*.twig',
      'components/*.css',
      'components/**/**/*.twig',
      'components/**/**/*.css'
    ],
  },
  theme: {
    screens: {
      // *** These are also .container max-width ***
      'xs': '375px', // => @media (min-width: 375px) { ... }
      'sm': '480px', // => @media (min-width: 480px) { ... }
      'md': '768px', // => @media (min-width: 768px) { ... }
      'lg': '1024px', // => @media (min-width: 1024px) { ... }
      'xl': '1280px', // => @media (min-width: 1280px) { ... }
    },
    fontFamily: {
      'readex-pro': '"readex pro", system-ui, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji"',
      'source-serif-pro': '"source serif pro", Georgia, Cambria, "Times New Roman", Times, serif',
      'sans': 'system-ui, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji"',
      'serif': 'Georgia, Cambria, "Times New Roman", Times, serif',
      'mono': 'Menlo, Monaco, Consolas, "Liberation Mono", "Courier New", monospace',
    },
    zIndex: {
      '1-negative': -1,
      '0': 0,
      '1': 1,
      '10': 10,
      '20': 20,
      '30': 30,
      '40': 40,
      '50': 50,
      'auto': 'auto',
    },
    colors: {
      black: '#000',
      white: '#fff',
      primary: '#0D3A65',
      secondary: '#1C768F',
      green: '#53D3D1',
      red: '#E60F5E',
      orange: '#FEB249',
      menuGray: '#E5E5E5',
      translucent: 'rgba(255, 255, 255, 0.5)',
      primaryLight: '#b2f5ea',
      primaryHover: '#319795',
      secondaryHover: colors.gray[600],
      accent: colors.gray[700],
      highlight: '#90cdf4',
      warning: '#faf089',
      message: '#9ae6b4',
      gray: colors.gray,
      transparent: 'transparent',
      current: 'currentColor',
    },
    extend: {
      borderWidth: {
        '6': '6px'
      },
      width: {
        '7px': '7px',
        '9px': '9px',
        '25': '25px',
        '9': '2.25rem',
        '300': '18.75rem',
        'sm': '24rem',
        'md': '32rem',
        'xl': '36rem',
        '2xl': '42rem',
        '1120': '70rem',
        '1440': '90rem',
        '39': '9.75rem',
        '44': '11rem',
        '65': '65%',
        '73': '18.25rem',
        '80': '20rem',
        '87': '21.75rem',
        'max': 'max-content',
        '30': '7.5rem',
        '130': '32.5rem',
        '158': '39.5rem',
        '1/4': '25%',
        '2/4': '50%',
        '3/4': '75%',
        '1/6': '16.666667%',
        '2/6': '33.333333%',
        '3/6': '50%',
        '4/6': '66.666667%',
        '5/6': '83.333333%',
        '1/8': '12.5%',
        '2/8': '25%',
        '3/8': '37.5%',
        '4/8': '60%',
        '5/8': '62.5%',
        '6/8': '75%',
        '7/8': '87.5%',
        '1/12': '8.333333%',
        '2/12': '16.666667%',
        '3/12': '25%',
        '4/12': '33.333333%',
        '5/12': '41.666667%',
        '6/12': '50%',
        '7/12': '58.333333%',
        '8/12': '66.666667%',
        '9/12': '75%',
        '10/12': '83.333333%',
        '11/12': '91.666667%',
      },
      height: {
        '2px': '2px',
        '3px': '3px',
        '7px': '7px',
        '9px': '9px',
        '9': '2.25rem',
        '13': '3.25rem',
        '92px': '5.75rem',
        '27': '6.75rem',
        '72': '18rem',
        '88': '22rem',
        '95': '23.75rem',
        '96': '24rem',
        '102': '26rem',
        '118': '30rem',
        '136': '34rem',
        '355': '22.1875rem',
        '385': '24.0625rem',
        '525': '32.8125rem',
        '1/4': '25%',
        '5/12': '41.6667%',
        '1/2': '50%',
        '7/12': '58.3333%',
        '3/4': '75%',
        '5/6': '83.3333%',
        'screen-1/2': '50vh',
        'screen-3/4': '75vh'
      },
      minWidth: {
        'button': '12.5rem', // ~200px.
        '25': '25%',
        '50': '50%',
        '65': '65%',
        '75': '75%',
      },
      minHeight: {
        '25': '25%',
        '50': '50%',
        '75': '75%',
        'vh-50': '50vh',
        '14': '3.5rem',
        '25r': '25rem',
        '355': '22.1875rem',
        '385': '24.0625rem',
        '1080': '67.5rem'
      },
      maxHeight: {
        '10': '10rem',
        '12': '3rem',
        '768': '48rem',
        '1080': '67.5rem',
        'vh-50': '50vh',
        'vh-75': '75vh',
      },
      maxWidth: {
        '2xl': '42rem',
        '71': '17.75rem',
        '100': '25rem',
        '220': '55rem',
        '284': '17.75rem',
        '468': '29.25rem',
        '510': '31.875rem',
        '206': '51.5rem',
      },
      margin: {
        '80-negative': '-20rem',
        '64-negative': '-16rem',
        '48-negative': '-12rem',
        '32-negative': '-8rem',
        '4px': '4px',
        '7': '1.75rem',
        '13': '3.25rem',
        '14': '3.5rem',
        '15': '3.75rem',
        '18': '4.5rem',
        '21': '5.25rem',
        '25': '6.25rem',
        '28': '7rem',
        '29': '7.25rem',
        '32': '8rem',
        '38': '8.75rem',
        '1/4': '25%',
        '2/4': '50%',
        '3/4': '75%',
        '1/6': '16.666667%',
        '2/6': '33.333333%',
        '3/6': '50%',
        '4/6': '66.666667%',
        '5/6': '83.333333%',
        '1/8': '12.5%',
        '2/8': '25%',
        '3/8': '37.5%',
        '4/8': '60%',
        '5/8': '62.5%',
        '6/8': '75%',
        '7/8': '87.5%',
        '1/12': '8.333333%',
        '2/12': '16.666667%',
        '3/12': '25%',
        '4/12': '33.333333%',
        '5/12': '41.666667%',
        '6/12': '50%',
        '7/12': '58.333333%',
        '8/12': '66.666667%',
        '9/12': '75%',
        '10/12': '83.333333%',
        '11/12': '91.666667%',
      },
      padding: {
        '6px': '6px',
        '13': '3.375rem',
        '14': '3.5rem',
        '15': '3.75rem',
        '30': '7.5rem',
        '90': '5.625rem',
        '110': '6.875rem',
        '28': '7rem',
        '34': '8.5rem',
        '1/3': '33.333334%',
        '1/2': '50%',
        '2/3': '66.666667%',
        '4/5': '80%',
        '93': '93%',
        'full': '100%',
        '1/4': '25%',
        '2/4': '50%',
        '3/4': '75%',
        '1/6': '16.666667%',
        '2/6': '33.333333%',
        '3/6': '50%',
        '4/6': '66.666667%',
        '5/6': '83.333333%',
        '1/8': '12.5%',
        '2/8': '25%',
        '3/8': '37.5%',
        '4/8': '60%',
        '5/8': '62.5%',
        '6/8': '75%',
        '7/8': '87.5%',
        '1/12': '8.333333%',
        '2/12': '16.666667%',
        '3/12': '25%',
        '4/12': '33.333333%',
        '5/12': '41.666667%',
        '6/12': '50%',
        '7/12': '58.333333%',
        '8/12': '66.666667%',
        '9/12': '75%',
        '10/12': '83.333333%',
        '11/12': '91.666667%',
      },
      inset: {
        '6px': '6px',
        '16px': '16px',
        '1/2-negative': '-50%',
        '1/10': '10%',
        '1/4': '25%',
        '1/2': '50%',
        'full': '100%'
      },
      borderRadius: {
        '1/2': '50%'
      },
      lineHeight: {
        '0': '0'
      },
      boxShadow: {
        'tiny': '0 0.25em 0.5em 0 rgba(0, 0, 0, 0.1)'
      },
      cursor: {
        'grab': 'grab',
        'grab-moz': '-moz-grab',
        'grab-wb': '-webkit-grab',
        'grabbing': 'grabbing',
        'grabbing-moz': '-moz-grabbing',
        'grabbing-wb': '-webkit-grabbing'
      },
      gridTemplateColumns: {
        '25-75': '1fr 2fr'
      },
      flex: {
        'basis-1/6': '0 0 16.6667%',
        'basis-1/3': '0 0 33%',
        'basis-1/2': '0 0 50%',
        'basis-2/3': '0 0 66%'
      },
      // @todo Adjust the Line height of the fonts, by default it is set to 1.5
      // @todo adjust fontsize for md,sm,xs breakpoints.
      fontSize: {
        // h1
        'heading-1-lg': ['2.625rem', '1.5'], // 50px - 1.5
        'heading-1-md': ['2.875rem', '1.5'], // 46px - 1.5
        'heading-1-sm': ['2.5rem', '1.5'], // 40px - 1.5
        'heading-1-xs': ['2.25rem', '1.5'], // 36px - 1.5
        // h2
        'heading-2-lg': ['2.625rem', '1.5'], // 42px - 1.5
        'heading-2-md': ['2.5rem', '1.5'], // 40px - 1.5
        'heading-2-sm': ['2.25rem', '1.5'], // 36px - 1.5
        'heading-2-xs': ['2.062rem', '1.5'], // 33px - 1.5
        // h3
        'heading-3-lg': ['2rem', '1.5'], // 38px - 1.5
        'heading-3-md': ['2.25rem', '1.5'], // 36px - 1.5
        'heading-3-sm': ['2.062rem', '1.5'], // 33px - 1.5
        'heading-3-xs': ['1.875rem', '1.5'], // 30px - 1.5
        // h4
        'heading-4-lg': ['1.625rem', '1.5'], // 33px - 1.5
        'heading-4-md': ['2.062rem', '1.5'], // 33px - 1.5
        'heading-4-sm': ['1.875rem', '1.5'], // 30px - 1.5
        'heading-4-xs': ['1.625rem', '1.5'], // 26px - 1.5
        // h5
        'heading-5-lg': ['1.375rem', '1.5'], // 28px - 1.5
        'heading-5-md': ['1.75rem', '1.5'], // 28px - 1.5
        'heading-5-sm': ['1.625rem', '1.5'], // 26px - 1.5
        'heading-5-xs': ['1.375rem', '1.5'], // 22px - 1.5
        // h6
        'heading-6-lg': ['1.375rem', '1.5'], // 22px - 1.5
        'heading-6-md': ['1.375rem', '1.5'], // 22px - 1.5
        'heading-6-sm': ['1.375rem', '1.5'], // 22px - 1.5
        'heading-6-xs': ['1.187rem', '1.5'], // 19px - 1.5
        // h7
        'heading-7-lg': ['1.125rem', '1.5'], // 22px - 1.5
        'heading-7-md': ['1.375rem', '1.5'], // 22px - 1.5
        'heading-7-sm': ['1.375rem', '1.5'], // 22px - 1.5
        'heading-7-xs': ['1.187rem', '1.5'], // 19px - 1.5
        // Summary
        'summary-lg': ['1.5rem', '1.5'], // 24px - 1.5
        'summary-md': ['1.5rem', '1.5'], // 24px - 1.5
        'summary-sm': ['1.312rem', '1.5'], // 21px - 1.5
        'summary-xs': ['1.312rem', '1.5'], // 21px - 1.5
        '5xl-1/2': '3.5rem',
        '6xl-1/2': '4.5rem',
        '7xl': '5rem',
        '7xl-1/2': '5.5rem',
        '8xl': '6rem',
        '8xl-1/2': '6.5rem',
        '9xl': '7rem',
        '9xl-1/2': '7.5rem',
        '10xl': '8rem',
        'semibase': ['0.9375rem', '2'], // ~15px
        'small': ['0.875rem', '1.5'], // ~14px
        'mini': ['0.8125rem', '1.5'] // ~13px
      },
      opacity: {
        '15': '.15',
        '60': '.6',
        '90': '.9'
      },
      translate: {
        '13/100-negative': '-13%',
        '23/100-negative': '-23%',
        '13/100': '13%',
        '23/100': '23%'
      }
    },
  },
  variants: {},
  plugins: [
    // https://github.com/tailwindlabs/tailwindcss-custom-forms
    // require('@tailwindcss/custom-forms'),
    // https://github.com/tailwindlabs/tailwindcss-typography
    require('@tailwindcss/typography'),
    require('./js/tailwind-col-count')
  ],
}
