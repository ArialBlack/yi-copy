// Documentation on theming Storybook: https://storybook.js.org/docs/configurations/theming/

import { create } from '@storybook/theming';

export default create({
  base: 'light',
  // Branding
  brandTitle: 'Freeflow By OPIN',
  brandUrl: 'https://www.opin.ca',
  brandImage:
    '',
});
