<?php

/**
 * @file
 * Theme template functions.
 *
 * Provides Twig template suggestions and variables
 * Based on Twig Suggest Project: https://www.drupal.org/project/twigsuggest/
 */

 use Drupal\block\Entity\Block;

/**
 * Implements hook_preprocess().
 *
 * @{inheritdoc}
 */
function yukon_immunize_preprocess(&$variables, $hook) {
  // The 'language' variable is supposed to be globally available in Twig
  // templates since Drupal 8.2, but sometimes it doesn't seem to work.
  // Force it here.
  $variables['language'] = \Drupal::languageManager()->getCurrentLanguage();
  // Tell all templates where they are located.
  $variables['directory'] = \Drupal::theme()->getActiveTheme()->getPath();
  // Allow the base path to be accessible via templates
  $variables['base_path'] = base_path();
}

/**
 * Implements hook_preprocess_media().
 *
 * @{inheritdoc}
 */
function yukon_immunize_preprocess_media(&$variables) {
  // Adding the media url to the variables.
  if (!is_null($variables['media']->id())) {
    $variables['url'] = $variables['media']->toUrl();
  }

  if (!empty($variables['elements']['#media'])) {
    $variables['attributes']['class'][] = 'media--' . str_replace('_', '-', $variables['elements']['#media']->bundle());
  }
}

/**
 * Implements hook_preprocess_user().
 *
 * @{inheritdoc}
 */
function yukon_immunize_preprocess_user(&$variables) {
  // Adding the users url to the variables.
  $variables['user_link'] = $variables['user']->toUrl();
}

/**
 * Implements hook_preprocess_hook().
 *
 * @{inheritdoc}
 */
function yukon_immunize_preprocess_field(&$variables) {
  if (isset($variables['element']['#field_name'])) {
    $field_values = $variables['element']['#items']->getValue();

    foreach ($field_values as $field_value) {
      if (isset($field_value['format']) && in_array($field_value['format'], ['full_html', 'basic_html', 'minimum_format'])) {
        $variables['attributes']['class'][] = 'formatted-text';
      }
    }
  }
}

/**
 * Implements hook_preprocess_region().
 *
 * @{inheritdoc}
 */
function yukon_immunize_preprocess_region(&$variables) {
  // Set theme settings to variables
  $logo = \Drupal::theme()->getActiveTheme()->getLogo();
  $config = \Drupal::config('system.site');

  $variables['theme_settings']['site_logo'] = base_path() . $logo;
  $variables['theme_settings']['site_name'] = $config->get('name');
  $variables['theme_settings']['site_slogan'] = $config->get('slogan');

  // Separate out region blocks into individual, themable items
  foreach($variables['elements'] as $block_key => $block) {
    if (substr($block_key, 0, 1) != '#' && isset($block['#plugin_id'])) {
      $variables['block'][$block['#plugin_id']] = $block;
    }
  }
}

/**
 * Implements hook_theme_suggestions_HOOK().
 *
 * Add additional template suggestion based on node type.
 */
function yukon_immunize_theme_suggestions_html(array $variables) {

  $node = FALSE;
  $route_match = \Drupal::routeMatch();

  if ($route_match->getRouteName() == 'entity.node.canonical') {
    $node = $route_match->getParameter('node');
    $suggestions[] = 'html__node__' . $node->getType();
  }
  elseif ($route_match->getRouteName() == 'entity.node.revision') {
    $revision_id = $route_match->getParameter('node_revision');
    $node = node_revision_load($revision_id);
    $suggestions[] = 'html__node__' . $node->getType();
  }
  elseif ($route_match->getRouteName() == 'entity.node.preview') {
    $node = $route_match->getParameter('node_preview');
    $suggestions[] = 'html__node__' . $node->getType();
  }
}

/**
 * Implements hook_theme_suggestions_HOOK().
 *
 */
function yukon_immunize_theme_suggestions_layout_alter(array &$suggestions, array $variables) {
  if ($variables['theme_hook_original'] == 'two_column' ) {
    if (in_array('search-results-container', $variables['content']['#attributes']['class'])) {
      $suggestions[] = 'layout__two_column__search';
    }
  }
}

/**
 * Implements hook_theme_suggestions_HOOK().
 *
 * Add additional template suggestion based on node type.
 */
function yukon_immunize_theme_suggestions_page(array $variables) {

  $node = FALSE;
  $route_match = \Drupal::routeMatch();

  if ($route_match->getRouteName() == 'entity.node.canonical') {
    $node = $route_match->getParameter('node');
    $suggestions[] = 'page__node__' . $node->getType();
  }
  elseif ($route_match->getRouteName() == 'entity.node.revision') {
    $revision_id = $route_match->getParameter('node_revision');
    $node = node_revision_load($revision_id);
    $suggestions[] = 'page__node__' . $node->getType();
  }
  elseif ($route_match->getRouteName() == 'entity.node.preview') {
    $node = $route_match->getParameter('node_preview');
    $suggestions[] = 'page__node__' . $node->getType();
  }
}

/**
 * Implements hook_theme_suggestions_HOOK_alter() for block templates.
 *
 * Suggest region-specific block templates.
 */
function yukon_immunize_theme_suggestions_block_alter(array &$suggestions, array $variables) {
  $route = \Drupal::routeMatch()->getRouteObject();

  if (!empty($route)) {
    $template_path_hook = implode('__', explode('/', $route->getPath()));

    if (!empty($variables['elements']['content']['#block_content'])) {
      // Creates template theme suggestion for custom block type.
      array_splice($suggestions, 1, 0, 'block__bundle__' . $variables['elements']['content']['#block_content']->bundle());
      // Get bundle/block type.
    }

    if (!empty($variables['elements']['#id'])) {
      $block = Block::load($variables['elements']['#id']);
      $suggestions[] = 'block__' . $block->getRegion();
      $suggestions[] = 'block__' . $block->getRegion() . '__' . $variables['elements']['#id'];
    }

    // Prevent PHP notices if contrib modules create blocks without this ID.
    if (isset($variables['elements']['#id']) || !empty($variables['elements']['#id'])) {

      if ($block = Block::load($variables['elements']['#id'])) {
        $suggestions[] = 'block__' . $block->getRegion() . $template_path_hook;
        $suggestions[] = 'block__' . $block->getRegion();
        $suggestions[] = 'block__' . $block->getRegion() . '__' . $variables['elements']['#id'];
        $suggestions[] = 'block__' . $block->getRegion() . '__' . $variables['elements']['#id'] . $template_path_hook;

        if ($block->get('settings') && $provider = $block->get('settings')['provider']) {
          // I'm pretty sure Drupal core is already providing provider as a suggestion.
          // $suggestions[] = 'block__' . $provider;
          $base_plugin = $variables['elements']['#base_plugin_id'];

          if ($base_plugin !== $provider) {
            $suggestions[] = 'block__' . $base_plugin;

            if ($template_path_hook) {
              $suggestions[] = 'block__' . $base_plugin . $template_path_hook;
            }
          }

          $suggestions[] = 'block__' . $provider . '__' . $block->getRegion();
          $suggestions[] = 'block__' . $provider . '__' . $block->getRegion() . $template_path_hook;

          if ($variables['elements']['#base_plugin_id'] != $provider) {
            $suggestions[] = 'block__' . $base_plugin . '__' . $block->getRegion();
            $suggestions[] = 'block__' . $base_plugin . '__' . $block->getRegion() . $template_path_hook;
          }

          if (isset($variables['elements']['content']['#menu_name'])) {
            $menu_name = $variables['elements']['content']['#menu_name'];
            $suggestions[] = 'block__' . $provider . '__' . $menu_name . '__' . $block->getRegion();
            $suggestions[] = 'block__' . $provider . '__' . $menu_name . '__' . $block->getRegion() . $template_path_hook;

            if ($base_plugin !== $provider) {
              $suggestions[] = 'block__' . $base_plugin . '__' . $menu_name . '__' . $block->getRegion();

              if ($template_path_hook) {
                $suggestions[] = 'block__' . $base_plugin . '__' . $menu_name . '__' . $block->getRegion() . $template_path_hook;
              }
            }
          }
        }
      }
    }

    // Remove duplicate suggestions
    $suggestions = array_unique($suggestions);
  }

}

/**
 * Implements hook_theme_suggestions_HOOK_alter() for container templates.
 *
 * Add suggestions, as by default none are provided.
 */
function yukon_immunize_theme_suggestions_container_alter(array &$suggestions, array $variables) {

  $element = $variables['element'];

  if (isset($element['#array_parents'])) {
    $suggestions[] = 'container__has_parent';
  }
  else {
    $suggestions[] = 'container__no_parent';
  }

  if (isset($element['#type']) && $element['#type'] != 'container') {
    $suggestions[] = 'container__' . $element['#type'];
  }

  if (isset($element['#type']) && $element['#type'] == 'container' && isset($element['children']['#type'])) {
    $suggestions[] = 'container__' . $element['children']['#type'];
  }

  if (isset($element['#type']) && $element['#type'] == 'view') {
    $suggestions[] = 'container__view__' . $element['#name'];
    $suggestions[] = 'container__view__' . $element['#name'] . '__' . $element['#display_id'];
  }
  elseif (isset($element['widget'][0]['#type']) && $element['widget'][0]['#type'] === 'managed_file') {
    $suggestions[] = 'container__file';
    $suggestions[] = 'container__file__' . $element['widget']['#field_name'];
  }

  // Additional module-specific container templates.
  if (isset($element['#group'])) {
    $suggestions[] = 'container__' . str_replace('-', '_', $element['#group']);
  }

  if (isset($element['#webform_key'])) {
    $suggestions[] = 'container__' . str_replace('-', '_', $element['#webform_key']);
  }
}

/**
 * Implements hook_theme_suggestions_HOOK_alter() for form element templates.
 *
 * Add suggestions, as by default none are provided.
 */
function yukon_immunize_theme_suggestions_form_alter(array &$suggestions, array $variables) {

  $element = $variables['element'];

  $route = \Drupal::routeMatch()->getRouteObject();
  $template_path_hook = implode('__', explode('/', $route->getPath()));

  if (isset($element['#id'])) {
    $suggestions[] = 'form__' . str_replace('-', '_', $element['#id']);
    $suggestions[] = 'form__' . str_replace('-', '_', $element['#id'] . $template_path_hook);
  }

  if (isset($element['#type'])) {
    $suggestions[] = 'form__' . str_replace('-', '_', $element['#type']);
    $suggestions[] = 'form__' . str_replace('-', '_', $element['#type'] . $template_path_hook);
  }
}

/**
 * Implements hook_theme_suggestions_HOOK_alter() for form element templates.
 *
 * Add suggestions, as by default none are provided.
 */
function yukon_immunize_theme_suggestions_form_element_alter(array &$suggestions, array $variables) {

  $element = $variables['element'];

  $route = \Drupal::routeMatch()->getRouteObject();
  $template_path_hook = implode('__', explode('/', $route->getPath()));

  if (isset($element['#id'])) {
    $suggestions[] = 'form_element__' . $element['#id'];
    $suggestions[] = 'form_element__' . $element['#id'] . $template_path_hook;
  }

  if (isset($element['#type'])) {
    $suggestions[] = 'form_element__' . $element['#type'];
    $suggestions[] = 'form_element__' . $element['#type'] . $template_path_hook;
  }

  // Additional module-specific container templates.
  if (isset($element['#webform_id'])) {
    $suggestions[] = 'form_element__webform__' . $element['#webform_id'];
    $suggestions[] = 'form_element__webform__' . $element['#webform_id'] . $template_path_hook;
  }
}

/**
 * Implements hook_theme_suggestions_HOOK_alter() for form region element templates.
 */
function yukon_immunize_theme_suggestions_region_alter(array &$suggestions, array $variables) {

  $element = $variables['elements'];

  $route = \Drupal::routeMatch()->getRouteObject();
  $template_path_hook = implode('__', explode('/', $route->getPath()));
  $root_path = explode("/", $route->getPath());

  if (isset($element['#region'])) {
    $suggestions[] = 'region__' . $element['#region'];
    $suggestions[] = 'region__' . $element['#region'] . $template_path_hook;
  }

  if (!empty($root_path)) {
    foreach ($root_path as $path) {
      if (!empty($path)) {
        $suggestions[] = 'region__' . $path;
      }
    }
  }
}

/**
 * Implements hook_theme_suggestions_HOOK_alter() for form input element templates.
 */
function yukon_immunize_theme_suggestions_input_alter(array &$suggestions, array $variables) {

  $element = $variables['element'];

  $route = \Drupal::routeMatch()->getRouteObject();
  $template_path_hook = implode('__', explode('/', $route->getPath()));

  if (isset($element['#id'])) {
    $suggestions[] = 'input__' . $element['#id'];
    $suggestions[] = 'input__' . $element['#id'] . $template_path_hook;
  }

  if (isset($element['#name'])) {
    $suggestions[] = 'input__' . $element['#name'];
    $suggestions[] = 'input__' . $element['#name'] . $template_path_hook;
  }

  if (isset($element['#type'])) {
    $suggestions[] = 'input__' . $element['#type'] . $template_path_hook;
  }
}


/**
 * Implements hook_theme_suggestions_HOOK_alter().
 *
 * Add template suggestions based on highest user role following the same
 * pattern as for nodes. @see https://www.drupal.org/node/2354645
 *
 * user--[role|uid]--[viewmode].html.twig
 */
function yukon_immunize_theme_suggestions_user_alter(array &$suggestions, array $variables) {

  $view_mode = $variables['elements']['#view_mode'];
  $mode = $variables['elements']['#view_mode'];

  /** @var \Drupal\user\Entity\User $user */
  $user = $variables['elements']['#user'];
  $roles = $user->getRoles();
  $highest_role = end($roles);
  $uid = $user->id();

  $suggestions[] = 'user__' . $uid;
  $suggestions[] = 'user__' . $view_mode;
  $suggestions[] = 'user__' . $highest_role;

  $suggestions[] = 'user__' . $uid . '__' . $view_mode;
  $suggestions[] = 'user__' . $highest_role . '__' . $view_mode;

  // Create a theme hook suggestion which has the view mode name in it.
  $suggestions[] = 'user__' . $mode;
}

/**
 * Implements hook_theme_suggestions_HOOK_alter().
 *
 * Add additional template suggestion based on node type.
 */
function yukon_immunize_theme_suggestions_menu_local_tasks_alter(array &$suggestions, array $variables) {

  $route = \Drupal::routeMatch()->getRouteObject();
  $template_path_hook = implode('__', explode('/', $route->getPath()));

  $suggestions[] = 'menu_local_tasks' . $template_path_hook;
}

/**
 * Implements hook_theme_suggestions_HOOK_alter().
 *
 * Add additional template suggestion based on node type.
 */
function yukon_immunize_theme_suggestions_menu_local_task_alter(array &$suggestions, array $variables) {

  $element = $variables['element'];

  $route = \Drupal::routeMatch()->getRouteObject();
  $template_path_hook = implode('__', explode('/', $route->getPath()));

  if (isset($element['#theme'])) {
    $suggestions[] = 'menu_local_task' . $template_path_hook;
  }
}

/**
 * Implements hook_theme_suggestions_HOOK_alter().
 */
function yukon_immunize_theme_suggestions_field_alter(array &$suggestions, array $variables) {

  $element = $variables['element'];

  $field_name = $element['#field_name'];
  $view_mode = $element['#view_mode'];
  $entity_type = $element['#entity_type'];
  $bundle = $element['#bundle'];

  $suggestions[] = 'field__' . $field_name . '__' . $view_mode;
  $suggestions[] = 'field__' . $entity_type . '__' . $field_name . '__' . $view_mode;
  $suggestions[] = 'field__' . $entity_type . '__' . $bundle . '__' . $field_name . '__' . $view_mode;
}

/**
 * Implements hook_theme_suggestions_HOOK_alter() for taxonomy terms.
 *
 * Currently Drupal core's taxonomy term module only provides:
 *  * $suggestions[] = 'taxonomy_term__' . $term->bundle();
 *  * $suggestions[] = 'taxonomy_term__' . $term->id() . $term->bundle();
 *
 * This is a very basic template suggestion that should be in core:
 * https://www.drupal.org/project/drupal/issues/2767243
 *
 */
function yukon_immunize_theme_suggestions_taxonomy_term_alter(array &$suggestions, array $variables) {
  $term = $variables['elements']['#taxonomy_term'];
  // We allow dots in view modes?!  But keeping this from discarded core patch.
  $sanitized_view_mode = strtr($variables['elements']['#view_mode'], '.', '_');
  $suggestions[] = 'taxonomy_term__' . $term->bundle() . '__' . $sanitized_view_mode;
  $suggestions[] = 'taxonomy_term__' . $sanitized_view_mode;
  $suggestions[] = 'taxonomy_term__' . $term->id() . '__' . $term->bundle() . '__' . $sanitized_view_mode;
  $suggestions[] = 'taxonomy_term__' . $term->id() . '__' . $sanitized_view_mode;
  $suggestions[] = 'taxonomy_term__' . $variables['elements']['name']['#bundle'] . '__' . $variables['elements']['#view_mode'];
}

/**
 * Implements hook_preprocess_HOOK().
 * this will allow pdf file to be opened with a new tab in browser.
 */
function yukon_immunize_preprocess_file_link(&$variables) {
  $variables['link']['#url']->setOption('attributes', ['target' => '_blank']);
}
