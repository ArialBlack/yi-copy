<?php

/**
 * @file
 * HTML template functions.
 */

use Drupal\block\Entity\Block;
use Drupal\Component\Utility\Html;

/**
 * Implements hook_preprocess_html().
 *
 * @{inheritdoc}
 */
function yukon_immunize_preprocess_html(&$variables) {
  // Add x-ua-compatible meta tag.
  $variables['page']['#attached']['html_head'][] = [
    [
      '#tag' => 'meta',
      '#attributes' => [
        'http-equiv' => 'x-ua-compatible',
        'content' => 'ie=edge',
      ],
    ],
    'x_ua_compatible',
  ];

  // Add a language to the body.
  $variables['language'] = \Drupal::languageManager()->getCurrentLanguage()->getId();

  $current_path = \Drupal::service('path.current')->getPath();
  $variables['is_layout_builder'] = (strpos($current_path, '/layout')) ? TRUE : FALSE;
  $variables['is_layout_builder_revert'] = (strpos($current_path, '/layout/revert')) ? TRUE : FALSE;
  $variables['is_layout_builder_discard'] = (strpos($current_path, '/layout/discard-changes')) ? TRUE : FALSE;

  $path_alias = \Drupal::service('path_alias.manager')->getAliasByPath($current_path);
  $path_args = explode('/', ltrim($path_alias, '/'));
  $variables['attributes']['class'][] = 'page--' . $path_args[0];
  $variables['attributes']['class'][] = 'page-' . Html::cleanCssIdentifier($path_alias);

  if (\Drupal::currentUser()->isAnonymous()) {
    $variables['attributes']['class'][] = 'not-logged-in';
  }

  // Add node type class.
  $node = \Drupal::routeMatch()->getParameter('node');

  if ($node) {
    $variables['attributes']['class'][] = 'path-node-' . $node->id();
    $variables['attributes']['class'][] = 'node-type-' . $node->getType();
  }

  // Add information about the number of sidebars to body.
  if (!empty(render($variables['page']['sidebar_first'])) && !empty(render($variables['page']['sidebar_second']))) {
    $variables['attributes']['class'][] = 'two-sidebars';
  }
  elseif (!empty(render($variables['page']['sidebar_first']))) {
    $variables['attributes']['class'][] = 'one-sidebar';
    $variables['attributes']['class'][] = 'sidebar-first';
  }
  elseif (!empty(render($variables['page']['sidebar_second']))) {
    $variables['attributes']['class'][] = 'one-sidebar';
    $variables['attributes']['class'][] = 'sidebar-second';
  }
  else {
    $variables['attributes']['class'][] = 'no-sidebars';
  }
}

/**
 * Implements hook_preprocess_hook().
 *
 * @{inheritdoc}
 */
function yukon_immunize_preprocess_layout (array &$variables) {
  $section_name = $variables['settings']['label'];
  $section_class_name = preg_replace('/\W+/','-',strtolower(strip_tags($section_name)));
  $variables['attributes']['class'][] = 'layout-section--' . $section_class_name;
}

/**
 * Implements hook_preprocess_page().
 *
 * @{inheritdoc}
 */
function yukon_immunize_preprocess_page(&$variables) {
  if (array_key_exists('node', $variables) && array_key_exists('layout_builder__layout', $variables['node']->getFields())) {
    $variables['layout_builder_in_use'] = TRUE;
  }
  else {
    $variables['layout_builder_in_use'] = FALSE;
  }
}

/**
 * Implements hook_preprocess_node().
 *
 * @{inheritdoc}
 */
function yukon_immunize_preprocess_node(&$variables) {
  $variables['lang_prefix'] = '';

  if (\Drupal::languageManager()->getCurrentLanguage()->getId() == 'fr') {
    $variables['lang_prefix'] = '/fr';
  }
}

/**
 * Implements hook_preprocess_block().
 *
 * @{inheritdoc}
 */
function yukon_immunize_preprocess_block(&$variables) {
  $variables['current_language'] = \Drupal::languageManager()->getCurrentLanguage();
  $variables['theme_path'] = base_path() . $variables['directory'];

  if (isset($variables['content']['#field_name'])) {
    $variables['attributes']['class'][] = 'field--' . str_replace('_', '-', $variables['content']['#field_name']);
  }
  if (isset($variables['content']['#field_type'])) {
    $variables['attributes']['class'][] = 'field-type--' . str_replace('_', '-', $variables['content']['#field_type']);
  }

  // Add block type class.
  if (!empty($variables['content']['#block_content'])) {
    // Get bundle/block type.
    $bundle = $variables['content']['#block_content']->bundle();
    // Set classname.
    $variables['attributes']['class'][] = 'block--' . str_replace('_', '-', $bundle);

    if (strpos($bundle, 'header') !== FALSE) {
      // Set theme settings to variables
      $logo = \Drupal::theme()->getActiveTheme()->getLogo();
      $config = \Drupal::config('system.site');

      // Language switcher.
      $block = Block::load('languageswitcher');
      if (!empty($block)) {
        $variables['language_switcher'] = \Drupal::entityTypeManager()
          ->getViewBuilder('block')
          ->view($block);
      }

      // To avoid accessibility issues a new language switcher is used for
      // mobile (to avoid duplicate IDs).
      $block = Block::load('languageswitcher_2');
      $variables['language_switcher_mobile'] = \Drupal::entityTypeManager()
        ->getViewBuilder('block')
        ->view($block);

      $variables['theme_settings']['site_logo'] = base_path() . $logo;
      $variables['theme_settings']['site_name'] = $config->get('name');
      $variables['theme_settings']['site_slogan'] = $config->get('slogan');
    }

    elseif (strpos($bundle, 'footer') !== FALSE) {
      $logo = \Drupal::theme()->getActiveTheme()->getLogo();

      $variables['site_logo'] = base_path() . $logo;
    }

    switch ($bundle) {
      case 'immunization_schedules_tables':
        $variables['#attached']['library'][] = 'yukon_immunize/basictable';
        $variables['#attached']['library'][] = 'yukon_immunize/immunization_schedules_tables';
        break;
    }
  }
  else {
    if (!empty($variables['base_plugin_id'])) {
      $variables['attributes']['class'][] = 'block--' . str_replace('_', '-', $variables['base_plugin_id']);
    }
  }
}

/**
 * Implements hook_attachments_alter()
 */
function yukon_immunize_page_attachments_alter(array &$attachments) {
  if (\Drupal::routeMatch()->getRouteName() == 'layout_builder.overrides.node.view') {
    $attachments['#attached']['library'][] = 'layout_builder_modal/seven';
  }
}
