<?php

/**
 * @file
 * Form template functions.
 */

use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_preprocess_bef_checkboxes().
 *
 * @{inheritdoc}
 */
function yukon_immunize_preprocess_bef_checkboxes(&$variables) {
  // Avoid displaying duplicate ids for checkboxes.
  $variables['attributes']['id'] = str_replace('--wrapper', '-bef--wrapper', $variables['attributes']['id']);
}

/**
 * Implements hook_preprocess_bef_radios().
 *
 * @{inheritdoc}
 */
function yukon_immunize_preprocess_bef_radios(&$variables) {
  // Avoid displaying duplicate ids for radios.
  $variables['attributes']['id'] = str_replace('--wrapper', '-bef--wrapper', $variables['attributes']['id']);
}

/**
 * Implements hook_form_views_exposed_form_alter().
 *
 * @{inheritdoc}
 */
function yukon_immunize_form_views_exposed_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  if ($form_id == 'views_exposed_form') {
    $form['Keyword']['#attributes']['placeholder'] = t('Find resources, videos, news, events and more ...');

    if (isset($form['field_faq_type_target_id'])) {
      $form['field_faq_type_target_id']['#select2'] = [
        'minimumResultsForSearch' => -1,
        'width' => '320px',
        'allowClear' => false,
      ];
    }
  }
}

/**
 * Implements hook_form_alter().
 *
 * @{inheritdoc}
 */
function yukon_immunize_form_alter(&$form, $form_state, $form_id) {
  // Adding placeholder and removing autocomplete for the constant contact block.
  if ($form_id == 'ik_constant_contact_sigup_form') {
    $form['email']['#attributes']['placeholder'] = t('Enter your email');
    $form['email']['#attributes']['autocomplete'] = 'off';
  }
}
